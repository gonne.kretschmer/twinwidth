package twinWidthNumber

import (
	"fmt"
	"testing"
)

// calculate the dth twin-width number, i.e. the smallest number n such that
// a graph with n nodes and twin-width d exists.
func TestTwinWidthNumber(t *testing.T) {
	twwNumbers := []int{2, 4, 5, 8, 9}
	for d, twwNumber := range twwNumbers {
		t.Run(fmt.Sprintf("Twin-width number %d", d), func(t *testing.T) {
			result := TwinWidthNumber(d, false)
			if result != twwNumber {
				t.Errorf("Wrong twinwidth number %d calculated: Got %d, expected %d.", d, result, twwNumber)
			}
		})
	}
}

func BenchmarkTwinWidthNumber(b *testing.B) {
	for d := 0; d < 4; d++ {
		b.Run(fmt.Sprintf("%d", d), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				TwinWidthNumber(d, false)
			}
		})
	}
}
