package twinWidthNumber

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path"
	"runtime"
	"sync"
	"twinWidth/graphProvider"
	"twinWidth/tww"

	"gonum.org/v1/gonum/graph"
)

// calculate the dth twin-width number, i.e. the smallest number n such that
// a graph with n nodes and twin-width d exists.
func TwinWidthNumber(d int, printResult bool) int {
	size := 2
	if d >= 5 { // There don't exist graphs of twin-width 5 on 10 nodes
		size = 11
	}
	for {
		var maxEdges int
		if size >= 4 { // for smaller graphs settinig maxEdges more restrictive doesn't work
			maxEdges = size * (size - 1) / 4 // A SAT Approach to Twin-Width Proposition 6.1
		} else {
			maxEdges = size * (size - 1) / 2
		}
		_, file, _, _ := runtime.Caller(0)
		if _, err := os.Stat(fmt.Sprintf("%s/../g6/%d-0-%d.g6", path.Dir(file), size, maxEdges)); err == nil {
			if printResult {
				fmt.Printf("Graphen von Größe %d sind schon gespeichert.\n", size)
			}
		} else if errors.Is(err, os.ErrNotExist) {
			generateCommand := exec.Command("bash", "-c", fmt.Sprintf("%s/../../nauty2_8_6/geng -c %d 0:%d > %s/../g6/%d-0-%d.g6", path.Dir(file), size, maxEdges, path.Dir(file), size, maxEdges))
			output, err := generateCommand.Output()
			if err != nil {
				fmt.Println(output)
				panic(err.Error())
			}
			if printResult {
				fmt.Printf("Generated Graphs with '%s/../../nauty2_8_6/geng -c %d 0:%d > %s/../g6/%d-0-%d.g6'", path.Dir(file), size, maxEdges, path.Dir(file), size, maxEdges)
			}
		} else {
			panic("Konnte nicht auf Graphendatei zugreifen.")
		}
		// Workers get graphs from this channel
		passGraph := make(chan graph.Graph, 2)
		success := false
		// Feed the workers with tournaments
		go graphProvider.NautyUnGraphsFile(fmt.Sprintf("%s/../g6/%d-0-%d.g6", path.Dir(file), size, maxEdges), passGraph)

		var graphsProcessed int
		var wg sync.WaitGroup
		for i := 0; i < runtime.NumCPU(); i++ {
			wg.Add(1)
			go func() {
				defer wg.Done()
				for g := range passGraph {
					above, _ := tww.TwinWidthAbove(g, d, d != 0)
					if above {
						success = true
						tww, sequence := tww.TwinWidthConnected(g)
						if printResult {
							fmt.Printf("%s	%d	%d	%v\n", g, g.Nodes().Len(), tww, sequence)
						}
					}
					if printResult {
						graphsProcessed++
						if graphsProcessed%1_000_000 == 0 {
							fmt.Printf("Processed %d graphs.\n", graphsProcessed)
						}
					}
				}
			}()
		}
		wg.Wait()
		if success {
			break
		}
		if printResult {
			fmt.Printf("Searched through all graphs of size %d.\n", size)
		}
		size++
	}
	return size
}
