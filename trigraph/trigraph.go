package trigraph

type Trigraph interface {
	Merge(int, int)
	Copy() Trigraph
	MaxNodes() int
	RedDegree() int
	Reset(Trigraph)
	Print()
	RemoveNode(int)
}
