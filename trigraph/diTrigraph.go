package trigraph

import (
	"fmt"
	"twinWidth/restrictedGraph"

	"gonum.org/v1/gonum/graph"
)

type DiTrigraph struct {
	Black *restrictedGraph.DiGraph
	Red   *restrictedGraph.UnGraphDegree
}

func (t *DiTrigraph) MaxNodes() int {
	return t.Black.MaxNodes()
}

func NewDiTrigraph(g graph.Directed) *DiTrigraph {
	black := restrictedGraph.NewDiGraph()
	red := restrictedGraph.NewUnGraphDegree()
	restrictedGraph.DiCopy(black, g)
	nodes := g.Nodes()
	for nodes.Next() {
		red.AddNode(int(nodes.Node().ID()))
	}
	return &DiTrigraph{Black: black, Red: red}
}

func NewDiTrigraphEmpty() *DiTrigraph {
	return &DiTrigraph{Black: restrictedGraph.NewDiGraph(), Red: restrictedGraph.NewUnGraphDegree()}
}

// Merge the second node into the first one.
func (t *DiTrigraph) Merge(u int, v int) {
	for node := 0; node < t.Black.MaxNodes(); node += 1 {
		if t.Red.Edge(node, v) && u != node {
			t.Red.SetEdge(node, u)
		}
		if (t.Black.Edge(node, u) != t.Black.Edge(node, v) || t.Black.Edge(u, node) != t.Black.Edge(v, node)) && node != u && node != v {
			t.Black.RemoveEdge(node, u)
			t.Black.RemoveEdge(node, v)
			t.Black.RemoveEdge(u, node)
			t.Black.RemoveEdge(v, node)
			t.Red.SetEdge(u, node)
		}
	}
	t.Black.RemoveNode(v)
	t.Red.RemoveNode(v)
}

func (t1 DiTrigraph) equals(t2 *DiTrigraph) bool {
	return t1.Black.Equals(t2.Black) && t1.Red.Equals(t2.Red)
}

func (t1 DiTrigraph) Equals(t2 *DiTrigraph) bool {
	return t1.Black.Equals(t2.Black) && t1.Red.Equals(t2.Red)
}

func (t *DiTrigraph) Copy() Trigraph {
	copy := NewDiTrigraphEmpty()
	restrictedGraph.CopyDiGraph(copy.Black, t.Black)
	restrictedGraph.CopyUnGraphDegree(copy.Red, t.Red)
	return copy
}

func (t *DiTrigraph) RedDegree() int {
	return t.Red.Degree()
}

func (t *DiTrigraph) Reset(g Trigraph) {
	t.Black.Reset(g.(*DiTrigraph).Black)
	t.Red.Reset(g.(*DiTrigraph).Red)
}

func (t *DiTrigraph) RemoveNode(n int) {
	t.Black.RemoveNode(n)
	t.Red.RemoveNode(n)
}

func (t *DiTrigraph) Print() {
	fmt.Println("Black:")
	restrictedGraph.PrintDiGraph(t.Black)

	fmt.Println("Red:")
	restrictedGraph.PrintUnGraphDegree(t.Red)
}
