package trigraph

import (
	"testing"
	"twinWidth/restrictedGraph"

	"gonum.org/v1/gonum/graph/encoding/digraph6"
)

// Verify that the sample config in config/config.json can be read to a config struct
// and passes the validation
func TestMerge(t *testing.T) {
	pyramid3result := NewDiTrigraph(digraph6.Graph("&BX?"))
	pyramid3result.Black.RemoveNode(1)
	pyramid3result.Red.RemoveNode(1)
	cycle3result2 := NewDiTrigraphEmpty()
	cycle3result2.Black.AddNode(0)
	cycle3result2.Black.AddNode(1)
	cycle3result2.Red.AddNode(0)
	cycle3result2.Red.AddNode(1)
	cycle3result2.Red.SetEdge(0, 1)
	cycle3result1 := NewDiTrigraphEmpty()
	cycle3result1.Black.AddNode(0)
	cycle3result1.Black.AddNode(2)
	cycle3result1.Red.AddNode(0)
	cycle3result1.Red.AddNode(2)
	cycle3result1.Red.SetEdge(0, 2)
	transitive4result := NewDiTrigraphEmpty()
	transitive4result.Black.AddNode(0)
	transitive4result.Black.AddNode(1)
	transitive4result.Black.AddNode(3)
	transitive4result.Red.AddNode(0)
	transitive4result.Red.AddNode(1)
	transitive4result.Red.AddNode(3)
	transitive4result.Black.SetEdge(0, 3)
	transitive4result.Black.SetEdge(1, 3)
	transitive4result.Red.SetEdge(0, 1)
	testCases := []struct {
		name      string
		testgraph *DiTrigraph
		merge     [2]int
		expected  *DiTrigraph
	}{
		{"3 pyramid contract 01", NewDiTrigraph(digraph6.Graph("&BX?")), [2]int{0, 1}, pyramid3result},
		{"3 cycle contract 01", NewDiTrigraph(digraph6.Graph("&BP_")), [2]int{0, 1}, cycle3result1},
		{"3 cycle contract 02", NewDiTrigraph(digraph6.Graph("&BP_")), [2]int{0, 2}, cycle3result2},
		{"4 transitive contract 02", NewDiTrigraph(digraph6.Graph("&C[p?")), [2]int{0, 2}, transitive4result}}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.testgraph.Merge(tc.merge[0], tc.merge[1])

			if !tc.testgraph.Equals(tc.expected) {
				t.Error("Graphen sind nicht gleich.")
				println("Actual black:")
				restrictedGraph.PrintDiGraph(tc.testgraph.Black)
				println("Expected black:")
				restrictedGraph.PrintDiGraph(tc.expected.Black)

				println("Actual red:")
				restrictedGraph.PrintUnGraphDegree(tc.testgraph.Red)
				println("Expected red:")
				restrictedGraph.PrintUnGraphDegree(tc.expected.Red)
			}
		})
	}
}

func TestSameEdges(t *testing.T) {
	cycle3result2 := NewDiTrigraphEmpty()
	cycle3result2.Black.AddNode(0)
	cycle3result2.Black.AddNode(1)
	cycle3result2.Red.AddNode(0)
	cycle3result2.Red.AddNode(1)
	cycle3result2.Red.SetEdge(0, 1)
	cycle3 := NewDiTrigraphEmpty()
	cycle3.Black.AddNode(0)
	cycle3.Black.AddNode(1)
	cycle3.Black.SetEdge(0, 2)
	cycle3.Black.AddNode(2)
	cycle3.Black.RemoveNode(2)
	cycle3.Red.AddNode(0)
	cycle3.Red.AddNode(1)
	cycle3.Red.AddNode(2)
	cycle3.Red.SetEdge(0, 1)
	cycle3.Red.RemoveNode(2)
	testCases := []struct {
		name      string
		trigraph1 *DiTrigraph
		trigraph2 *DiTrigraph
		sameEdges bool
	}{
		{"3 pyramid and no edges", NewDiTrigraph(digraph6.Graph("&BX?")), &DiTrigraph{
			Black: restrictedGraph.NewDiGraph(),
			Red:   restrictedGraph.NewUnGraphDegree(),
		}, false},
		{"3 pyramid and 3 pyramid", NewDiTrigraph(digraph6.Graph("&BX?")), NewDiTrigraph(digraph6.Graph("&BX?")), true},
		{"3 cycle and no edges", NewDiTrigraph(digraph6.Graph("&BP_")), &DiTrigraph{
			Black: restrictedGraph.NewDiGraph(),
			Red:   restrictedGraph.NewUnGraphDegree(),
		}, false},
		{"3 cycle and 3 cycle", NewDiTrigraph(digraph6.Graph("&BP_")), NewDiTrigraph(digraph6.Graph("&BP_")), true},
		{"3 cycle and 3 pyramid", NewDiTrigraph(digraph6.Graph("&BP_")), NewDiTrigraph(digraph6.Graph("&BX?")), false},
		{"3 cycle contracted", cycle3result2, cycle3, true},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			result := tc.trigraph1.Equals(tc.trigraph2)
			if result != tc.sameEdges {
				t.Errorf("sameEdges() ergab %t, aber erwartet wurde %t.", result, tc.sameEdges)
			}
			result = tc.trigraph2.Equals(tc.trigraph1)
			if result != tc.sameEdges {
				t.Errorf("sameEdges() ergab %t, aber erwartet wurde %t.", result, tc.sameEdges)
			}
		})
	}
}

func TestRedDegree(t *testing.T) {
	pyramid3result := NewDiTrigraph(digraph6.Graph("&BX?"))
	pyramid3result.Black.RemoveNode(1)
	pyramid3result.Red.RemoveNode(1)
	cycle3result := NewDiTrigraphEmpty()
	cycle3result.Black.AddNode(0)
	cycle3result.Black.AddNode(1)
	cycle3result.Red.AddNode(0)
	cycle3result.Red.AddNode(1)
	cycle3result.Red.SetEdge(0, 1)
	transitive4result := NewDiTrigraphEmpty()
	transitive4result.Black.AddNode(0)
	transitive4result.Black.AddNode(1)
	transitive4result.Black.AddNode(3)
	transitive4result.Red.AddNode(0)
	transitive4result.Red.AddNode(1)
	transitive4result.Red.AddNode(3)
	transitive4result.Black.SetEdge(0, 3)
	transitive4result.Black.SetEdge(1, 3)
	transitive4result.Red.SetEdge(0, 1)
	testCases := []struct {
		name      string
		trigraph  *DiTrigraph
		redDegree int
	}{
		{"3 pyramid after 01 contraction", pyramid3result, 0},
		{"3 cycle after 02 contraction", cycle3result, 1},
		{"4 transitive after 02 contraction", transitive4result, 1}}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if tc.trigraph.RedDegree() != tc.redDegree {
				t.Errorf("Red degree wrong. Expected %d, got %d.", tc.redDegree, tc.trigraph.RedDegree())
				println("red graph:")
				restrictedGraph.PrintUnGraphDegree(tc.trigraph.Red)
			}
		})
	}
}
