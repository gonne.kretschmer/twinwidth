package trigraph

import (
	"fmt"
	"twinWidth/restrictedGraph"

	"gonum.org/v1/gonum/graph"
)

type UnTrigraph struct {
	Black *restrictedGraph.UnGraph
	Red   *restrictedGraph.UnGraphDegree
}

func (t UnTrigraph) MaxNodes() int {
	return t.Black.MaxNodes()
}

func NewUnTrigraph(g graph.Undirected) *UnTrigraph {
	black := restrictedGraph.NewUnGraph()
	red := restrictedGraph.NewUnGraphDegree()
	restrictedGraph.UnCopy(black, g)
	nodes := g.Nodes()
	for nodes.Next() {
		red.AddNode(int(nodes.Node().ID()))
	}
	return &UnTrigraph{Black: black, Red: red}
}

func NewUnTrigraphEmpty() *UnTrigraph {
	return &UnTrigraph{Black: restrictedGraph.NewUnGraph(), Red: restrictedGraph.NewUnGraphDegree()}
}

// Merge the second node into the first one.
func (t *UnTrigraph) Merge(u int, v int) {
	for node := 0; node < t.Black.MaxNodes(); node += 1 {
		if node != u && node != v {
			if t.Red.Edge(node, v) {
				t.Red.SetEdge(node, u)
				t.Black.RemoveEdge(node, u)
			} else if t.Black.Edge(node, u) != t.Black.Edge(node, v) {
				t.Black.RemoveEdge(node, u)
				t.Black.RemoveEdge(node, v)
				t.Red.SetEdge(u, node)
			}
		}
	}
	t.Black.RemoveNode(v)
	t.Red.RemoveNode(v)
}

func (t1 UnTrigraph) Equals(t2 *UnTrigraph) bool {
	return t1.Black.Equals(t2.Black) && t1.Red.Equals(t2.Red)
}

func (t UnTrigraph) Copy() Trigraph {
	copy := NewUnTrigraphEmpty()
	restrictedGraph.CopyUnGraph(copy.Black, t.Black)
	restrictedGraph.CopyUnGraphDegree(copy.Red, t.Red)
	return copy
}

func (t *UnTrigraph) RedDegree() int {
	return t.Red.Degree()
}

func (t *UnTrigraph) RemoveNode(n int) {
	t.Black.RemoveNode(n)
	t.Red.RemoveNode(n)
}

func (t *UnTrigraph) Reset(g Trigraph) {
	t.Black.Reset(g.(*UnTrigraph).Black)
	t.Red.Reset(g.(*UnTrigraph).Red)
}

func (t *UnTrigraph) Print() {
	fmt.Println("Black:")
	restrictedGraph.PrintUnGraph(t.Black)

	fmt.Println("Red:")
	restrictedGraph.PrintUnGraphDegree(t.Red)
}
