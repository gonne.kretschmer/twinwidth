package graphProvider

import (
	"fmt"
	"testing"
	"twinWidth/tww"
)

//Test, if Paley-9 has Twin-Width 4
func TestPaley9(t *testing.T) {
	t.Parallel()
	result, sequence := tww.TwinWidthConnected(Paley9())
	if result != 4 {
		t.Errorf("Twinwidth calculated wrongly: Got %d, expected %d", result, 12)
	}
	if len(sequence) != Paley9().Nodes().Len()-1 {
		t.Errorf("Unexpected contraction sequence length: Got %v, expected %d", sequence, Paley25().Nodes().Len()-1)
	}
}

//Test, if Paley-25 has Twin-Width 12
func TestPaley25(t *testing.T) {
	t.Parallel()
	result, sequence := tww.TwinWidthConnected(Paley25())
	if result != 12 {
		t.Errorf("Twinwidth calculated wrongly: Got %d, expected %d", result, 12)
	}
	if len(sequence) != Paley25().Nodes().Len()-1 {
		t.Errorf("Unexpected contraction sequence length: Got %v, expected %d", sequence, Paley25().Nodes().Len()-1)
	}
}

func TestPrimePaley(t *testing.T) {
	primes := []int{5, 13, 17, 29, 37, 41, 53, 61}
	for _, prime := range primes {
		t.Run(fmt.Sprintf("Paley-%d", prime), func(t *testing.T) {
			t.Parallel()
			result, sequence := tww.TwinWidthConnected(PrimePaley(prime))
			if result != (prime-1)/2 {
				t.Errorf("Twinwidth calculated wrongly: Got %d, expected %d", result, (prime-1)/2)
			}
			if len(sequence) != PrimePaley(prime).Nodes().Len()-1 {
				t.Errorf("Unexpected contraction sequence length: Got %v, expected %d", sequence, PrimePaley(prime).Nodes().Len()-1)
			}
		})
	}
}

//Test, if Paley-49 has Twin-Width 24
func TestPaley49(t *testing.T) {
	t.Parallel()
	result, sequence := tww.TwinWidthConnected(Paley49())
	if result != 24 {
		t.Errorf("Twinwidth calculated wrongly: Got %d, expected %d", result, 24)
	}
	if len(sequence) != Paley49().Nodes().Len()-1 {
		t.Errorf("Unexpected contraction sequence length: Got %v, expected %d", sequence, Paley49().Nodes().Len()-1)
	}
}
