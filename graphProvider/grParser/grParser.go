// grParser.go
package grParser

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/simple"
)

// Recieves a PACE gr file and converts its content to an undirected graph.
// Format description is available at https://pacechallenge.org/2023/io/
func UnGraphFromDIMACSFile(file string) graph.Undirected {
	readFile, err := os.Open(file)
	defer readFile.Close()
	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)
	return UnGraphFromDIMACSScanner(fileScanner)
}

// Recieves a PACE graph as a string in a scanner and converts its content to an undirected graph.
// Format description is available at https://pacechallenge.org/2023/io/
func UnGraphFromDIMACSScanner(scanner *bufio.Scanner) graph.Undirected {
	scanner.Split(bufio.ScanLines)

	result := simple.NewUndirectedGraph()
	for scanner.Scan() {
		line := scanner.Text()
		if line != "" {
			if line[0] == 'p' {
				arguments := strings.Split(line, " ")
				nodes, _ := strconv.Atoi(arguments[2])
				for node := 1; node <= nodes; node += 1 {
					result.AddNode(simple.Node(node))
				}
			} else if line[0] != 'c' {
				pair := strings.Split(line, " ")
				x, _ := strconv.Atoi(pair[0])
				y, _ := strconv.Atoi(pair[1])
				// Don't add self-loops
				if x != y {
					result.SetEdge(simple.Edge{
						F: simple.Node(x),
						T: simple.Node(y),
					})
				}
			}
		}
	}
	return result
}

// Recieves a graph with nodes from 1 to n and converts it to a DIMACS-string.
func DIMACSFromGraph(g graph.Graph) string {
	var (
		output   string
		edges    int
		directed bool
	)
	switch g.(type) {
	case graph.Directed:
		directed = true
	case graph.Undirected:
		directed = false
	}
	nodes := g.Nodes()
	for nodes.Next() {
		node := nodes.Node()
		neighbours := g.From(node.ID())
		for neighbours.Next() {
			neighbour := neighbours.Node()
			if node.ID() < neighbour.ID() || directed {
				output = output + fmt.Sprintf("e %d %d\n", node.ID(), neighbour.ID())
				edges += 1
			}
		}
	}
	output = fmt.Sprintf("p edge %d %d\n", g.Nodes().Len(), edges) + output
	return output
}
