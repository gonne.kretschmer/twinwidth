package grParser

import (
	"testing"
)

// Test the parser of GR graphs.
func TestUnGraphFromGR(t *testing.T) {
	tc := []struct {
		file  string
		nodes int
		edges int
	}{
		{"../../gr/tiny-set/selfloop.gr", 10, 9},
		{"../../gr/tiny-set/comments.gr", 10, 9},
		{"../../gr/tiny-set/tiny001.gr", 10, 9},
		{"../../gr/tiny-set/tiny002.gr", 10, 10},
		{"../../gr/tiny-set/tiny003.gr", 10, 45},
		{"../../gr/tiny-set/tiny004.gr", 10, 9},
		{"../../gr/tiny-set/tiny005.gr", 25, 40},
		{"../../gr/tiny-set/tiny006.gr", 10, 5},
		{"../../gr/tiny-set/tiny007.gr", 25, 24},
		{"../../gr/tiny-set/tiny008.gr", 10, 15},
		{"../../gr/tiny-set/tiny009.gr", 9, 12},
		{"../../gr/tiny-set/tiny010.gr", 20, 55},
	}
	for _, test := range tc {
		t.Run(test.file, func(t *testing.T) {
			graph := UnGraphFromDIMACSFile(test.file)
			if graph.Nodes().Len() != test.nodes {
				t.Errorf("Got %d nodes, expected %d.", graph.Nodes().Len(), test.nodes)
			}
			var count int
			nodes := graph.Nodes()
			for nodes.Next() {
				node := nodes.Node()
				neighbours := graph.From(node.ID())
				for neighbours.Next() {
					if neighbours.Node().ID() > node.ID() {
						count += 1
					}
				}
			}
			if count != test.edges {
				t.Errorf("Got %d edges, expected %d.", count, test.edges)
			}
		})
	}
}
