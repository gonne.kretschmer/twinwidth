package graphProvider

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"sync"
	"twinWidth/graphProvider/grParser"
	"twinWidth/tww"

	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/encoding/digraph6"
	"gonum.org/v1/gonum/graph/encoding/graph6"
)

// calculate twinwidth of all graphs passed via channel
// and print it together with the graph to stdout
func CalulateMass(passGraph chan (graph.Graph), printResult bool) {
	if printResult {
		fmt.Println("Graph	Knoten	twinWidth	ContractionSequence")
	}

	var wg sync.WaitGroup
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for g := range passGraph {
				tww, sequence := tww.TwinWidth(g)
				if printResult {
					fmt.Printf("%s	%d	%d	%v\n", g, g.Nodes().Len(), tww, sequence)
				}
			}
		}()
	}
	wg.Wait()
}

// calculate twinwidth of all graphs in files passed via channel
// and print it together with the file to stdout
func CalulateMassFile(passFile chan (string), printResult bool) {
	if printResult {
		fmt.Println("Graph	Knoten	twinWidth	ContractionSequence")
	}

	var wg sync.WaitGroup
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for file := range passFile {
				g := grParser.UnGraphFromDIMACSFile(file)
				tww, sequence := tww.TwinWidth(g)
				if printResult {
					fmt.Printf("%s	%d	%d	%v\n", file, g.Nodes().Len(), tww, sequence)
				}
			}
		}()
	}
	wg.Wait()
}

// Recieves a Nauty command and converts its output to directed graphs
// They are passed into the pass channel.
// Intended to be run in a separate go routine.
func NautyDiGraphs(generateCommand *exec.Cmd, passGraph chan (graph.Graph)) {
	output, err := generateCommand.Output()
	if err != nil {
		fmt.Println(output)
		panic(err.Error())
	}
	outputBuffer := bufio.NewReader(bytes.NewBuffer(output))
	var line string
	for err == nil {
		line, err = outputBuffer.ReadString('\n')
		line = strings.Trim(line, "\n")
		if line != "" {
			passGraph <- digraph6.Graph(line)
		}
	}
	close(passGraph)
}

// Recieves a Nauty command and converts its output to undirected graphs.
// They are passed into the pass channel.
// Intended to be run in a separate go routine.
func NautyUnGraphs(generateCommand *exec.Cmd, passGraph chan (graph.Graph)) {
	output, err := generateCommand.Output()
	if err != nil {
		fmt.Println(output)
		panic(err.Error())
	}
	outputBuffer := bufio.NewReader(bytes.NewBuffer(output))
	var line string
	for err == nil {
		line, err = outputBuffer.ReadString('\n')
		line = strings.Trim(line, "\n")
		if line != "" {
			passGraph <- graph6.Graph(line)
		}
	}
	close(passGraph)
}

// Recieves a Nauty g6 file and converts its content to undirected graphs.
// They are passed into the pass channel.
// Intended to be run in a separate go routine.
func NautyUnGraphsFile(file string, passGraph chan (graph.Graph)) {
	readFile, err := os.Open(file)

	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)
	for fileScanner.Scan() {
		line := fileScanner.Text()
		if line != "" {
			passGraph <- graph6.Graph(line)
		}
	}
	readFile.Close()
	close(passGraph)
}

// Recieves a Nauty d6 file and converts its content to undirected graphs.
// They are passed into the pass channel.
// Intended to be run in a separate go routine.
func NautyDiGraphsFile(file string, passGraph chan (graph.Graph)) {
	readFile, err := os.Open(file)

	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		line := fileScanner.Text()
		if line != "" {
			passGraph <- digraph6.Graph(line)
		}
	}
	readFile.Close()
	close(passGraph)
}
