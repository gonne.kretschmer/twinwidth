package graphProvider

import (
	"fmt"
	"testing"
)

func BenchmarkTournaments(b *testing.B) {
	for nodes := 2; nodes < 9; nodes++ {
		b.Run(fmt.Sprintf("%d", nodes), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				CalculateTournaments([]int{nodes}, false)
			}
		})
	}
}
