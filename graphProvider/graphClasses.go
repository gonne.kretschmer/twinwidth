package graphProvider

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path"
	"runtime"

	"gonum.org/v1/gonum/graph"
)

// calculate twinwidth of all connected tournaments of a given size (number of nodes)
// and print it together with the tournament to stdout
func CalculateTournaments(sizes []int, printResult bool) {
	_, file, _, _ := runtime.Caller(0)
	for _, size := range sizes {
		fmt.Printf("%s/../../nauty2_8_6/gentourng -z -c %d", path.Dir(file), size)
		generateCommand := exec.Command("bash", "-c", fmt.Sprintf("%s/../../nauty2_8_6/gentourng -z -c %d ", path.Dir(file), size))
		passGraph := make(chan (graph.Graph), 2)
		go NautyDiGraphs(generateCommand, passGraph)
		CalulateMass(passGraph, printResult)
	}
}

// calculate twinwidth of all connected undirected graphs of a given size (number of nodes)
// and print it together with the graph to stdout
func CalculateGraphs(sizes []int, printResult bool) {
	_, file, _, _ := runtime.Caller(0)
	for _, size := range sizes {
		if _, err := os.Stat(fmt.Sprintf("%s/../g6/%d.g6", path.Dir(file), size)); err == nil {
			if printResult {
				fmt.Printf("Graphen von Größe %d sind schon gespeichert.\n", size)
			}
		} else if errors.Is(err, os.ErrNotExist) {
			generateCommand := exec.Command("bash", "-c", fmt.Sprintf("%s/../../nauty2_8_6/geng -c %d > %s/../g6/%d.g6", path.Dir(file), size, path.Dir(file), size))
			output, err := generateCommand.Output()
			if err != nil {
				fmt.Println(output)
				panic(err.Error())
			}
			if printResult {
				fmt.Printf("Generated Graphs with '../nauty2_8_6/geng -c %d > g6/%d.g6'\n", size, size)
			}
		} else {
			panic("Konnte nicht auf Graphendatei zugreifen.")
		}
		// Workers get graphs from this channel
		passGraph := make(chan graph.Graph, 2)
		// Feed the workers with tournaments
		go NautyUnGraphsFile(fmt.Sprintf("%s/../g6/%d.g6", path.Dir(file), size), passGraph)

		CalulateMass(passGraph, printResult)
	}
}
