package contraction

import (
	"fmt"
	"testing"
	"twinWidth/trigraph"

	"gonum.org/v1/gonum/graph/encoding/digraph6"
)

func TestDegreeFromSequence(t *testing.T) {
	testCases := []struct {
		name     string
		trigraph *trigraph.DiTrigraph
		sequence ContractionSequence
		degree   int
	}{
		{"3 pyramid", trigraph.NewDiTrigraph(digraph6.Graph("&BX?")), ContractionSequence{{0, 1}, {0, 2}}, 0},
		{"3 pyramid", trigraph.NewDiTrigraph(digraph6.Graph("&BX?")), ContractionSequence{{0, 2}, {0, 1}}, 1},
		{"3 cycle", trigraph.NewDiTrigraph(digraph6.Graph("&BP_")), ContractionSequence{{0, 1}, {0, 2}}, 1},
		{"4 transitive", trigraph.NewDiTrigraph(digraph6.Graph("&C[p?")), ContractionSequence{{0, 2}, {0, 1}, {0, 3}}, 1},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if DegreeFromSequence(tc.trigraph, tc.sequence) != tc.degree {
				t.Errorf("d-Sequence calculated wrongly: Got %d, expected %d", DegreeFromSequence(tc.trigraph, tc.sequence), tc.degree)
			}
		})
	}
}

func TestPaceFormat(t *testing.T) {
	testCases := []struct {
		cs     ContractionSequence
		result string
	}{
		{ContractionSequence{}, ""},
		{ContractionSequence{{0, 1}, {0, 2}}, "0 1\n0 2"},
		{ContractionSequence{{0, 2}, {0, 1}, {0, 3}}, "0 2\n0 1\n0 3"},
	}
	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%v", tc.cs), func(t *testing.T) {
			if tc.cs.PaceFormat() != tc.result {
				t.Errorf("Wrong PACE format: Got '%s', but expected '%s'.", tc.cs.PaceFormat(), tc.result)
			}
		})
	}
}
