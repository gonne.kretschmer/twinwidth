// partition.go
package contraction

type PartitionSlice struct {
	store []*DegreePartition
}

func NewPartitionSlice() *PartitionSlice {
	return &PartitionSlice{store: make([]*DegreePartition, 0)}
}

func (s *PartitionSlice) Add(p Partition, d int) bool {
searchPartition:
	for i, partition := range s.store {
		if len(partition.p) != len(p) {
			continue
		}
		for node, set := range partition.p {
			if p[node] != set {
				continue searchPartition
			}
		}
		if s.store[i].d > d {
			s.store[i].d = d
			return true
		}
		return false
	}
	s.store = append(s.store, &DegreePartition{p: p, d: d})
	return true
}

func (s *PartitionSlice) Contains(p Partition) bool {
searchPartition:
	for _, partition := range s.store {
		if len(partition.p) != len(p) {
			continue
		}
		for node, set := range partition.p {
			if p[node] != set {
				continue searchPartition
			}
		}
		return true
	}
	return false
}
