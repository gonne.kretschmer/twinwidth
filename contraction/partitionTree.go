// partition.go
package contraction

import (
	"runtime"
)

type PartitionTree struct {
	root  *partitionNode // at each level the equivalence class of the respective vertex is saved
	nodes int
}

type partitionNode struct {
	degree   int
	children []*partitionNode
}

func NewPartitionTree() *PartitionTree {
	return &PartitionTree{root: &partitionNode{0, make([]*partitionNode, 0)}, nodes: 1}
}

func (t *PartitionTree) Add(p Partition, d int) bool {
	if t.nodes*(len(p)+1)+len(p)*len(p) > 500_000_000 { // don't let the cache grow larger as there is not enough memory
		t.root = &partitionNode{0, make([]*partitionNode, 0)}
		t.nodes = 1
		go runtime.GC()
	}

	accept := false
	node := t.root
	for _, equivalenceClass := range p {
		if equivalenceClass < len(node.children) && node.children[equivalenceClass] != nil {
			node = node.children[equivalenceClass]
		} else {
			accept = true
			for equivalenceClass >= len(node.children) {
				node.children = append(node.children, nil)
			}
			node.children[equivalenceClass] = &partitionNode{0, make([]*partitionNode, 0)}
			node = node.children[equivalenceClass]
			t.nodes += 1
		}
	}
	if accept || node.degree > d {
		accept = true
		node.degree = d
	}
	return accept
}

func (t *PartitionTree) Contains(p Partition) bool {
	node := t.root
	for _, equivalenceClass := range p {
		if equivalenceClass < len(node.children) && node.children[equivalenceClass] != nil {
			node = node.children[equivalenceClass]
		} else {
			return false
		}
	}
	return true
}
