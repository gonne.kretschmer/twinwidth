// contractionSequence
package contraction

import (
	"fmt"
	"os"
	"strings"
	"twinWidth/trigraph"
)

type Contraction [2]int                // Merge the second node into the first node
type ContractionSequence []Contraction // Sequence of contractions

// Determine minimum d for which a given contraction sequence
// on a graph is a d-sequence.
func DegreeFromSequence(t trigraph.Trigraph, s ContractionSequence) int {
	n := t.MaxNodes()
	return FastDegreeFromSequence(t, s, n)
}

// Get degree of contraction sequence.
// Return early if degree reaches a maximum degree.
func FastDegreeFromSequence(t trigraph.Trigraph, s ContractionSequence, maxDegree int) int {
	var degree int
	for _, contraction := range s {
		t.Merge(contraction[0], contraction[1])
		if t.RedDegree() > degree {
			degree = t.RedDegree()
		}
		if degree >= maxDegree {
			return degree
		}
	}
	return degree
}

// Apply a contraction sequence to a given trigraph
func ApplySequence(t trigraph.Trigraph, s ContractionSequence) trigraph.Trigraph {
	workGraph := t.Copy()
	for _, contraction := range s {
		workGraph.Merge(contraction[0], contraction[1])
	}
	return workGraph
}

func (cs ContractionSequence) Partition(n int) Partition {
	result := make(Partition, n)
	for i, _ := range result {
		result[i] = i
	}
	for _, contraction := range cs {
		for i := contraction[1]; i < len(result); i++ {
			if result[i] == contraction[1] {
				result[i] = contraction[0]
			}
		}
	}
	return result
}

// Return a contraction sequence in the PACE-Challenge format.
// https://pacechallenge.org/2023/io/
func (cs ContractionSequence) PaceFormat() string {
	var result string
	for _, contraction := range cs {
		result += fmt.Sprintf("%d %d\n", contraction[0], contraction[1])
	}
	result = strings.TrimSuffix(result, "\n")
	return result
}

func (cs ContractionSequence) WriteToFile(file string) {
	os.WriteFile(file, []byte(cs.PaceFormat()), 0644)
}
