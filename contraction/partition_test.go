package contraction

import (
	"testing"
)

func CoreTestPartition(t *testing.T, cache PartitionCache) {
	partitions := []Partition{
		Partition{0, 1, 2, 3, 4},
		Partition{0, 0, 2, 3, 4},
		Partition{0, 1, 0, 3, 4},
		Partition{0, 1, 2, 0, 4},
		Partition{0, 1, 2, 3, 0},
		Partition{0, 0, 2, 0, 4},
	}
	for _, partition := range partitions {
		if cache.Contains(partition) {
			t.Errorf("Expected not to find partition %v, but found.\n", partition)
		}
	}
	for _, partition := range partitions {
		if !cache.Add(partition, 10) {
			t.Errorf("Expected to be able to add new partition %v to cache.\n", partition)
		}
	}
	for _, partition := range partitions {
		if !cache.Add(partition, 9) {
			t.Errorf("Expected to be able to improve partition %v from degree 10 to 9.\n", partition)
		}
	}
	for _, partition := range partitions {
		if cache.Add(partition, 9) {
			t.Errorf("Expected to reject partition %v from degree 9 as it exists with degree 9.\n", partition)
		}
	}
	for _, partition := range partitions {
		if cache.Add(partition, 10) {
			t.Errorf("Expected to reject partition %v with degree 10 as it exists with degree 9.\n", partition)
		}
	}
	for _, partition := range partitions {
		if !cache.Contains(partition) {
			t.Errorf("Expected to find partition %v, but not found.\n", partition)
		}
	}
}

func TestPartitionSlice(t *testing.T) {
	CoreTestPartition(t, NewPartitionSlice())
}

func TestPartitionTree(t *testing.T) {
	CoreTestPartition(t, NewPartitionTree())
}
