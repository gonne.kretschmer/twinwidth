// partition.go
package contraction

import (
	"fmt"
)

type Partition []int //at each index (node) the minimal node of its equivalence class

type DegreePartition struct {
	p Partition
	d int
}

type PartitionCache interface {
	Add(Partition, int) bool
	Contains(Partition) bool
}

type PartitionCombi struct {
	slice *PartitionSlice
	tree  *PartitionTree
}

func NewPartitionCombi() *PartitionCombi {
	return &PartitionCombi{slice: NewPartitionSlice(), tree: NewPartitionTree()}
}

func (c *PartitionCombi) Add(p Partition, d int) bool {
	result := c.slice.Add(p, d)
	if result != c.tree.Add(p, d) {
		fmt.Printf("Ungleich: p: %v, d: %d, slice: %t\n", p, d, result)
	}
	return result
}

func (c *PartitionCombi) Contains(p Partition) bool {
	result := c.slice.Contains(p)
	if result != c.tree.Contains(p) {
		fmt.Printf("Ungleich p: %v, slice: %t\n", p, result)
	}
	return result
}
