package restrictedGraph

import (
	"gonum.org/v1/gonum/graph"
)

// UnGraphDegree implements an undirected graph based on an adjecency matrix and caches the degree.
type UnGraphDegree struct {
	deleted  []bool
	degree   []int
	numNodes int //number of (potentially deleted) nodes in the graph (is one larger than than the node with largest id).

	// this slice holds whether for n < m the nodes n and m are adjecent as a bool in position
	neighbours []bool
}

// NewUnGraphDegree returns an UnGraphDegree.
func NewUnGraphDegree() *UnGraphDegree {
	return &UnGraphDegree{
		deleted:    make([]bool, 0),
		degree:     make([]int, 0),
		numNodes:   0,
		neighbours: make([]bool, 0),
	}
}

// AddNode adds n to the graph.
func (g *UnGraphDegree) AddNode(n int) {
	if g.numNodes > n { // set node undeleted if it exists as deleted
		g.deleted[n] = false
	} else {
		for len(g.neighbours) < (n+1)*n/2 {
			g.neighbours = append(g.neighbours, false)
		}
		for g.numNodes < n { // add nodes smaller than n as deleted
			g.numNodes += 1
			g.deleted = append(g.deleted, true)
			g.degree = append(g.degree, 0)
		}
		g.numNodes += 1 // add n
		g.deleted = append(g.deleted, false)
		g.degree = append(g.degree, 0)
	}
}

// Edge returns if there is an edge from u to v.
func (g *UnGraphDegree) Edge(uid, vid int) bool {
	if uid > vid {
		uid, vid = vid, uid
	}
	/*if vid >= g.numNodes {
		return false
	}*/
	if uid == vid {
		return false
	}
	return g.neighbours[(vid)*(vid-1)/2+uid]
}

// HasEdgeBetween returns whether an edge exists between nodes x and y.
func (g *UnGraphDegree) HasEdgeBetween(xid, yid int) bool {
	return g.Edge(xid, yid)
}

// HasEdgeFromTo returns whether an edge exists in the graph from u to v.
func (g *UnGraphDegree) HasEdgeFromTo(uid, vid int) bool {
	return g.Edge(uid, vid)
}

// Nodes returns all the nodes in the graph.
func (g *UnGraphDegree) Nodes() []int {
	result := make([]int, 0, g.numNodes)
	for i := 0; i < g.numNodes; i++ {
		if !g.deleted[i] {
			result = append(result, i)
		}
	}
	return result
}

// RemoveEdge removes the edge with the given end point IDs from the graph, leaving the terminal
// nodes. If the edge does not exist it is a no-op.
func (g *UnGraphDegree) RemoveEdge(fid, tid int) {
	if fid > tid {
		tid, fid = fid, tid
	}
	if fid != tid && g.Edge(fid, tid) {
		g.degree[fid] -= 1
		g.degree[tid] -= 1
		g.neighbours[(tid)*(tid-1)/2+fid] = false
	}
}

// RemoveNode removes the node with the given ID from the graph, as well as any edges attached
// to it. If the node is not in the graph it is a no-op.
func (g *UnGraphDegree) RemoveNode(id int) {
	if g.numNodes <= id || g.deleted[id] {
		return
	}

	for other := 0; other < g.numNodes; other++ {
		g.RemoveEdge(id, other)
	}
	g.deleted[id] = true
}

// SetEdge adds an edge from fid to tid. If the nodes do not exist, they are added.
func (g *UnGraphDegree) SetEdge(fid, tid int) {
	g.AddNode(fid)
	g.AddNode(tid)
	if fid > tid {
		tid, fid = fid, tid
	}
	if fid != tid && !g.Edge(fid, tid) {
		g.degree[fid] += 1
		g.degree[tid] += 1
		g.neighbours[(tid)*(tid-1)/2+fid] = true
	}
}

// Degree returns the degree (i.e. the maximum neighbourhood size of a node) of a graph.
func (g *UnGraphDegree) Degree() int {
	var max int
	for _, degree := range g.degree {
		if degree > max {
			max = degree
		}
	}
	return max
}

// CopyUnGraphDegree copies an UnGraphDegree into another one by copying the underlying memory.
func CopyUnGraphDegree(dst *UnGraphDegree, src *UnGraphDegree) {
	dst.deleted = make([]bool, len(src.deleted))
	copy(dst.deleted, src.deleted)

	dst.degree = make([]int, len(src.degree))
	copy(dst.degree, src.degree)

	dst.numNodes = src.numNodes

	dst.neighbours = make([]bool, len(src.neighbours))
	copy(dst.neighbours, src.neighbours)
}

// Equals checks whether two UnGraphDegrees are equal (as existing nodes and edges between them).
// Exact representation in memory doesn't matter.
func (t1 *UnGraphDegree) Equals(t2 *UnGraphDegree) bool { // implemented unefficiently, only needed for tests
	t1nodes := t1.Nodes()
	t2nodes := t2.Nodes()
	if len(t1nodes) != len(t2nodes) {
		return false
	}
	for i := 0; i < len(t1nodes); i++ {
		if t1nodes[i] != t2nodes[i] {
			return false
		}
	}
	for _, node1 := range t1nodes {
		for _, node2 := range t1nodes {
			if t1.HasEdgeFromTo(node1, node2) != t2.HasEdgeFromTo(node1, node2) {
				return false
			}
		}
	}
	return true
}

// UnDegreeCopy copies a generic undirected graph as defined by gonum/graph into an UnGraphDegree.
func UnDegreeCopy(dst *UnGraphDegree, src graph.Graph) {
	nodes := src.Nodes()
	for nodes.Next() {
		dst.AddNode(int(nodes.Node().ID()))
	}
	nodes.Reset()
	for nodes.Next() {
		u := nodes.Node()
		uid := u.ID()
		to := src.From(uid)
		for to.Next() {
			v := to.Node()
			dst.SetEdge(int(uid), int(v.ID()))
		}
	}
}

// Reset sets t1 to the state of t2 by copying the underlying memory.
func (t1 *UnGraphDegree) Reset(t2 *UnGraphDegree) {
	copy(t1.deleted, t2.deleted)
	copy(t1.neighbours, t2.neighbours)
	copy(t1.degree, t2.degree)
	t1.numNodes = t2.numNodes
}
