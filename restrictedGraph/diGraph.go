package restrictedGraph

import (
	"gonum.org/v1/gonum/graph"
)

// DiGraph implements a directed graph based on an adjecency matrix.
type DiGraph struct {
	deleted  []bool
	numNodes int // number of (potentially deleted) nodes in the graph (is one larger than than the node with largest id).

	// this slice holds whether there is an edge from n to m as a bool in position n*numNodes+m
	neighbours []bool
}

// NewDiGraph returns a DiGraph.
func NewDiGraph() *DiGraph {
	return &DiGraph{
		deleted:    make([]bool, 0),
		numNodes:   0,
		neighbours: make([]bool, 0),
	}
}

// MaxNodes gives an upper bound to the number of nodes in the graph.
// A common usage is to iterate over all nodes up to this number.
func (g *DiGraph) MaxNodes() int {
	return g.numNodes
}

// AddNode adds n to the graph.
func (g *DiGraph) AddNode(n int) {
	if g.numNodes > n { // set node undeleted if it exists as deleted
		g.deleted[n] = false
	} else {
		for g.numNodes < n { // add nodes smaller than n as deleted
			g.numNodes += 1
			g.deleted = append(g.deleted, true)
		}
		g.numNodes += 1 // add n
		g.deleted = append(g.deleted, false)
		for len(g.neighbours) < g.numNodes*g.numNodes {
			g.neighbours = append(g.neighbours, false)
		}
	}
}

// Edge returns if there is an edge from u to v.
func (g *DiGraph) Edge(uid, vid int) bool {
	if vid >= g.numNodes {
		return false
	}
	if uid == vid {
		return false
	}
	return g.neighbours[uid*g.numNodes+vid]
}

// HasEdgeBetween returns whether an edge exists between nodes x and y without
// considering direction.
func (g *DiGraph) HasEdgeBetween(xid, yid int) bool {
	return g.Edge(xid, yid) || g.Edge(yid, xid)
}

// HasEdgeFromTo returns whether an edge exists in the graph from u to v.
func (g *DiGraph) HasEdgeFromTo(uid, vid int) bool {
	return g.Edge(uid, vid)
}

// Nodes returns all the nodes in the graph as an array.
func (g *DiGraph) Nodes() []int {
	result := make([]int, 0, g.numNodes)
	for i := 0; i < g.numNodes; i++ {
		if !g.deleted[i] {
			result = append(result, i)
		}
	}
	return result
}

// RemoveEdge removes the edge with the given end point IDs from the graph, leaving the terminal
// nodes. If the edge does not exist it is a no-op.
// Caution: if you are currently iterating over g.neighbours, you must reset your index.
func (g *DiGraph) RemoveEdge(fid, tid int) {
	g.RemoveDirectedEdge(fid, tid)
}

func (g *DiGraph) RemoveDirectedEdge(fid, tid int) {
	if fid != tid {
		g.neighbours[fid*g.numNodes+tid] = false
	}
}

// RemoveNode removes the node with the given ID from the graph, as well as any edges attached
// to it. If the node is not in the graph it is a no-op.
func (g *DiGraph) RemoveNode(id int) {
	if g.numNodes <= id {
		return
	}
	for other := 0; other < g.numNodes; other++ {
		g.RemoveEdge(id, other)
		g.RemoveEdge(other, id)
	}
	g.deleted[id] = true
}

// SetEdge adds an edge from fid to tid. If the nodes do not exist, they are added.
func (g *DiGraph) SetEdge(fid, tid int) {
	g.AddNode(fid)
	g.AddNode(tid)
	g.neighbours[fid*g.numNodes+tid] = true
}

// CopyDiGraph copies a DiGraph into another one.
func CopyDiGraph(dst *DiGraph, src *DiGraph) {
	dst.deleted = make([]bool, len(src.deleted))
	copy(dst.deleted, src.deleted)

	dst.numNodes = src.numNodes

	dst.neighbours = make([]bool, len(src.neighbours))
	copy(dst.neighbours, src.neighbours)
}

// Equals checks whether two DiGraphs are equal (as existing nodes and edges between them).
// Exact representation in memory doesn't matter.
func (t1 *DiGraph) Equals(t2 *DiGraph) bool { // implemented unefficiently, only needed for tests
	t1nodes := t1.Nodes()
	t2nodes := t2.Nodes()
	if len(t1nodes) != len(t2nodes) {
		return false
	}
	for i := 0; i < len(t1nodes); i++ {
		if t1nodes[i] != t2nodes[i] {
			return false
		}
	}
	for _, node1 := range t1nodes {
		for _, node2 := range t1nodes {
			if t1.HasEdgeFromTo(node1, node2) != t2.HasEdgeFromTo(node1, node2) {
				return false
			}
		}
	}
	return true
}

// DiCopy copies a generic directed graph as defined by gonum/graph into a DiGraph.
func DiCopy(dst *DiGraph, src graph.Directed) {
	nodes := src.Nodes()
	for nodes.Next() {
		dst.AddNode(int(nodes.Node().ID()))
	}
	nodes.Reset()
	for nodes.Next() {
		u := nodes.Node()
		uid := u.ID()
		to := src.From(uid)
		for to.Next() {
			v := to.Node()
			dst.SetEdge(int(uid), int(v.ID()))
		}
	}
}

// Reset sets t1 to the state of t2 by copying the underlying memory.
func (t1 *DiGraph) Reset(t2 *DiGraph) {
	copy(t1.deleted, t2.deleted)
	copy(t1.neighbours, t2.neighbours)
	t1.numNodes = t2.numNodes
}
