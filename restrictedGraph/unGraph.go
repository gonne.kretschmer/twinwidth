package restrictedGraph

import (
	"gonum.org/v1/gonum/graph"
)

// UnGraph implements an undirected graph based on an adjecency matrix
type UnGraph struct {
	deleted  []bool
	numNodes int //number of (potentially deleted) nodes in the graph (is one larger than than the node with largest id).

	// this slice holds whether for n < m the nodes n and m are adjecent as a bool in position
	neighbours []bool
}

// MaxNodes gives an upper bound to the number of nodes in the graph.
// A common usage is to iterate over all nodes up to this number.
func (g *UnGraph) MaxNodes() int {
	return g.numNodes
}

// NewUnGraph returns an UnGraph.
func NewUnGraph() *UnGraph {
	return &UnGraph{
		deleted:    make([]bool, 0),
		numNodes:   0,
		neighbours: make([]bool, 0),
	}
}

// AddNode adds n to the graph.
func (g *UnGraph) AddNode(n int) {
	if g.numNodes > n { // set node undeleted if it exists as deleted
		g.deleted[n] = false
	} else {
		for len(g.neighbours) < (n+1)*n/2 {
			g.neighbours = append(g.neighbours, false)
		}
		for g.numNodes < n { // add nodes smaller than n as deleted
			g.numNodes += 1
			g.deleted = append(g.deleted, true)
		}
		g.numNodes += 1 // add n
		g.deleted = append(g.deleted, false)
	}

}

// Edge returns if there is an edge from u to v.
func (g *UnGraph) Edge(uid, vid int) bool {
	if uid > vid {
		uid, vid = vid, uid
	}
	/*if vid >= g.numNodes {
		return false
	}*/
	if uid == vid {
		return false
	}
	return g.neighbours[(vid)*(vid-1)/2+uid]
}

// HasEdgeBetween returns whether an edge exists between nodes x and y without
// considering direction.
func (g *UnGraph) HasEdgeBetween(xid, yid int) bool {
	return g.Edge(xid, yid)
}

// HasEdgeFromTo returns whether an edge exists in the graph from u to v.
func (g *UnGraph) HasEdgeFromTo(uid, vid int) bool {
	return g.Edge(uid, vid)
}

// Nodes returns all the nodes in the graph.
func (g *UnGraph) Nodes() []int {
	result := make([]int, 0, g.numNodes)
	for i := 0; i < g.numNodes; i++ {
		if !g.deleted[i] {
			result = append(result, i)
		}
	}
	return result
}

// RemoveEdge removes the edge with the given end point IDs from the graph, leaving the terminal
// nodes. If the edge does not exist it is a no-op.
func (g *UnGraph) RemoveEdge(fid, tid int) {
	if fid > tid {
		tid, fid = fid, tid
	}
	if fid != tid {
		g.neighbours[(tid)*(tid-1)/2+fid] = false
	}
}

// RemoveNode removes the node with the given ID from the graph, as well as any edges attached
// to it. If the node is not in the graph it is a no-op.
func (g *UnGraph) RemoveNode(id int) {
	if g.numNodes <= id {
		return
	}
	for other := 0; other < g.numNodes; other++ {
		g.RemoveEdge(id, other)
	}
	g.deleted[id] = true
}

// SetEdge adds an edge from fid to tid. If the nodes do not exist, they are added.
func (g *UnGraph) SetEdge(fid, tid int) {
	g.AddNode(fid)
	g.AddNode(tid)
	if fid > tid {
		tid, fid = fid, tid
	}
	g.neighbours[(tid)*(tid-1)/2+fid] = true
}

// Returns the degree (i.e. the maximum neighbourhood size of a node) of a graph.
// This is expensive when done often, consider using UnGraphDegree.
func (g *UnGraph) Degree() int {
	var max int
	degrees := make([]int, g.numNodes)
	for m := 1; m < g.numNodes; m += 1 {
		for n := 0; n < m; n += 1 {
			if g.neighbours[(m)*(m-1)/2+n] {
				degrees[m] += 1
				degrees[n] += 1
			}
		}
	}
	for _, degree := range degrees {
		if degree > max {
			max = degree
		}
	}
	return max
}

// CopyUnGraph copies an UnGraph into another one by copying the underlying memory.
func CopyUnGraph(dst *UnGraph, src *UnGraph) {
	dst.deleted = make([]bool, len(src.deleted))
	copy(dst.deleted, src.deleted)

	dst.numNodes = src.numNodes

	dst.neighbours = make([]bool, len(src.neighbours))
	copy(dst.neighbours, src.neighbours)
}

// Equals checks whether two UnGraphs are equal (as existing nodes and edges between them).
// Exact representation in memory doesn't matter.
func (t1 *UnGraph) Equals(t2 *UnGraph) bool { // implemented unefficiently, only needed for tests
	t1nodes := t1.Nodes()
	t2nodes := t2.Nodes()
	if len(t1nodes) != len(t2nodes) {
		return false
	}
	for i := 0; i < len(t1nodes); i++ {
		if t1nodes[i] != t2nodes[i] {
			return false
		}
	}
	for _, node1 := range t1nodes {
		for _, node2 := range t1nodes {
			if t1.HasEdgeFromTo(node1, node2) != t2.HasEdgeFromTo(node1, node2) {
				return false
			}
		}
	}
	return true
}

// UnCopy copies a generic undirected graph as defined by gonum/graph into an UnGraph.
func UnCopy(dst *UnGraph, src graph.Undirected) {
	nodes := src.Nodes()
	for nodes.Next() {
		dst.AddNode(int(nodes.Node().ID()))
	}
	nodes.Reset()
	for nodes.Next() {
		u := nodes.Node()
		uid := u.ID()
		to := src.From(uid)
		for to.Next() {
			v := to.Node()
			dst.SetEdge(int(uid), int(v.ID()))
		}
	}
}

// Reset sets t1 to the state of t2 by copying the underlying memory.
func (t1 *UnGraph) Reset(t2 *UnGraph) {
	copy(t1.deleted, t2.deleted)
	copy(t1.neighbours, t2.neighbours)
	t1.numNodes = t2.numNodes
}
