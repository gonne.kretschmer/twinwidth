package restrictedGraph

import (
	"fmt"

	"gonum.org/v1/gonum/graph"
)

func PrintGraph(g graph.Graph) {
	nodes := g.Nodes()
	for nodes.Next() {
		node := nodes.Node()
		fmt.Printf("Node: %d, neighbours: ", node.ID())
		neighbours := g.From(node.ID())
		for neighbours.Next() {
			fmt.Printf(" %d ", neighbours.Node().ID())
		}
		fmt.Println()
	}
	fmt.Println()
}

func PrintUnGraph(g *UnGraph) {
	for node := 0; node < g.numNodes; node += 1 {
		if !g.deleted[node] {
			fmt.Printf("Node: %d, neighbours: ", node)

			for neighbour := 0; neighbour < g.numNodes; neighbour += 1 {
				if g.Edge(node, neighbour) {
					fmt.Printf(" %d ", neighbour)
				}
			}
			fmt.Println()
		}
	}
	fmt.Printf("deleted: %v, neighbours: %v\n", g.deleted, g.neighbours)
	fmt.Println()
}

func PrintDiGraph(g *DiGraph) {
	for node := 0; node < g.numNodes; node += 1 {
		if !g.deleted[node] {
			fmt.Printf("Node: %d, neighbours: ", node)

			for neighbour := 0; neighbour < g.numNodes; neighbour += 1 {
				if g.Edge(node, neighbour) {
					fmt.Printf(" %d ", neighbour)
				}
			}
			fmt.Println()
		}
	}
	fmt.Printf("deleted: %v, neighbours: %v\n", g.deleted, g.neighbours)
	fmt.Println()
}

func PrintUnGraphDegree(g *UnGraphDegree) {
	for node := 0; node < g.numNodes; node += 1 {
		if !g.deleted[node] {
			fmt.Printf("Node: %d, neighbours: ", node)

			for neighbour := 0; neighbour < g.numNodes; neighbour += 1 {
				if g.Edge(node, neighbour) {
					fmt.Printf(" %d ", neighbour)
				}
			}
			fmt.Println()
		}
	}
	fmt.Printf("Degrees: %v\n", g.degree)
	fmt.Println()
}
