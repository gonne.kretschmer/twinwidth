package tww

import (
	"fmt"
	"os/exec"
	"strconv"
	"strings"
	"testing"
	"twinWidth/contraction"
	"twinWidth/graphProvider/grParser"
	"twinWidth/trigraph"

	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/encoding/digraph6"
	"gonum.org/v1/gonum/graph/encoding/graph6"
)

func TestTwinWidth(t *testing.T) {
	testCases := []struct {
		name      string
		graph     graph.Graph
		twinWidth int
	}{
		{"3 pyramid", digraph6.Graph("&BX?"), 0}, // Tests verified by hand, directed graphs.
		{"3 cycle", digraph6.Graph("&BP_"), 1},
		{"4 transitive", digraph6.Graph("&C[p?"), 0},
		{"Beispiel 1, das den ersten Algorithmus kaputt macht.", digraph6.Graph("&EWLEbpg"), 1},
		{"Beispiel 2, das den ersten Algorithmus kaputt macht.", digraph6.Graph("&EZKFaPS"), 1},

		{"C4 + 1 node", graph6.Graph("DIK"), 0},
		{"C3 + P2", graph6.Graph("D`K"), 0},
		{"3 path", graph6.Graph("BW"), 0}, //hand verified, connected undirected graphs of 3 vertices
		{"3 cycle", graph6.Graph("Bw"), 0},
		{"Claw graph", graph6.Graph("CF"), 0}, // connected graphs of 4 vertices
		{"P4", graph6.Graph("CU"), 1},
		{"Paw graph", graph6.Graph("CV"), 0},
		{"4 cycle", graph6.Graph("C]"), 0},
		{"Diamond graph", graph6.Graph("C^"), 0},
		{"K4", graph6.Graph("C~"), 0},
		{"Fork graph", graph6.Graph("D@s"), 1}, // connected graphs of 5 vertices
		{"Banner graph", graph6.Graph("DBw"), 1},
		{"Bull graph", graph6.Graph("DD["), 1},
		{"Graph 246", graph6.Graph("DFw"), 0},
		{"Graph 276", graph6.Graph("DF{"), 0},
		{"P5", graph6.Graph("DDW"), 1},
		{"Graph 330", graph6.Graph("D`["), 1},
		{"5 cycle", graph6.Graph("DqK"), 2},
		{"Graph 420", graph6.Graph("DJ{"), 0},
		{"Graph 438", graph6.Graph("Dr["), 0},
		{"Graph 442", graph6.Graph("Dr{"), 0},
		{"Graph 450", graph6.Graph("D^{"), 0},
		{"Pentatope Graph", graph6.Graph("D~{"), 0},
		{"Graph 544", graph6.Graph("D?{"), 0},
		{"Butterfly Graph", graph6.Graph("D`{"), 0},
		{"Kite graph", graph6.Graph("DJk"), 1},
		{"House X graph", graph6.Graph("DN{"), 0},
		{"Cricket graph", graph6.Graph("D@{"), 0},
		{"Dart graph", graph6.Graph("DB{"), 0},
		{"Gem graph", graph6.Graph("DR{"), 1},
		{"House graph", graph6.Graph("Dd["), 1},

		{"", digraph6.Graph("&@?"), 0}, // result is not expected to change.
		{"", digraph6.Graph("&BP_"), 1},
		{"", digraph6.Graph("&COx_"), 1},
		{"", digraph6.Graph("&DW[[W?"), 1},
		{"", digraph6.Graph("&DY[SQ?"), 1},
		{"", digraph6.Graph("&DYW[S?"), 1},
		{"", digraph6.Graph("&DWW[[?"), 1},
		{"", digraph6.Graph("&DY[[@?"), 1},
		{"", digraph6.Graph("&D[SYW?"), 1},
		{"", digraph6.Graph("&E[NFB`_"), 1},
		{"", digraph6.Graph("&E\\MFB`O"), 1},
		{"", digraph6.Graph("&E\\NEB`G"), 1},
		{"", digraph6.Graph("&E\\NFA`C"), 1},
		{"", digraph6.Graph("&E\\NFB_A"), 1},
		{"", digraph6.Graph("&E]LFBP_"), 1},
		{"", digraph6.Graph("&E]NDBH_"), 1},
		{"", digraph6.Graph("&EWNFb`_"), 1},
		{"", digraph6.Graph("&EXMFb`O"), 1},
		{"", digraph6.Graph("&EXNEb`G"), 1},
		{"", digraph6.Graph("&EXNFa`C"), 1},
		{"", digraph6.Graph("&EXNFb_A"), 1},
		{"", digraph6.Graph("&EWNEb`g"), 1},
		{"", digraph6.Graph("&EWNFa`c"), 1},
		{"", digraph6.Graph("&EXMEb`W"), 1},
		{"", digraph6.Graph("&EXMFa`S"), 1},
		{"", digraph6.Graph("&EYLFbP_"), 1},
		{"", digraph6.Graph("&EZKFbPO"), 1},
		{"", digraph6.Graph("&EZLEbPG"), 2},
		{"", digraph6.Graph("&EZLFaPC"), 1},
		{"", digraph6.Graph("&EZLFbOA"), 1},
		{"", digraph6.Graph("&EZKFaPS"), 1},
		{"", digraph6.Graph("&EZKEbPW"), 1},
		{"", digraph6.Graph("&EYLEbPg"), 2},
		{"", digraph6.Graph("&EWLFbp_"), 1},
		{"", digraph6.Graph("&EWKFbpo"), 1},
		{"", digraph6.Graph("&EWLEbpg"), 1},
		{"", digraph6.Graph("&E[JFR`_"), 1},
		{"", digraph6.Graph("&E\\IFR`O"), 1},
		{"", digraph6.Graph("&E\\JFR_A"), 1},
		{"", digraph6.Graph("&ESNfB`_"), 1},
		{"", digraph6.Graph("&ETNeB`G"), 1},
		{"", digraph6.Graph("&ETNfA`C"), 1},
		{"", digraph6.Graph("&ETMfB`O"), 1},
		{"", digraph6.Graph("&ETNfB_A"), 1},
		{"", digraph6.Graph("&F]f`w[F@O?"), 1},
		{"", digraph6.Graph("&F]fpo[F@G?"), 1},
		{"", digraph6.Graph("&F]Fpw[F@_?"), 1},
		{"", digraph6.Graph("&F]fpwWF@C?"), 1},
		{"", digraph6.Graph("&F]fpw[D@A?"), 1},
		{"", digraph6.Graph("&F]fpw[F?@?"), 1},
		{"", digraph6.Graph("&F^FPw[E`_?"), 1},
		{"", digraph6.Graph("&F^Fpg[EP_?"), 1},
		{"", digraph6.Graph("&F^FpwSEH_?"), 1},
		{"", digraph6.Graph("&F[Fpw]F@_?"), 1},
		{"", digraph6.Graph("&F[f`w]F@O?"), 1},
		{"", digraph6.Graph("&F[fpo]F@G?"), 1},
		{"", digraph6.Graph("&F[fpw]D@A?"), 1},
		{"", digraph6.Graph("&F[fpw]F?@?"), 1},
		{"", digraph6.Graph("&F[fpwYF@C?"), 1},
		{"", digraph6.Graph("&F[FpwYF@c?"), 1},
		{"", digraph6.Graph("&F[Fpw]D@a?"), 1},
		{"", digraph6.Graph("&F[f`w]D@Q?"), 1},
		{"", digraph6.Graph("&F[fpo]D@I?"), 1},
		{"", digraph6.Graph("&F\\FPw]E`_?"), 1},
		{"", digraph6.Graph("&F\\f@w]E`O?"), 1},
		{"", digraph6.Graph("&F\\fPo]E`G?"), 2},
		{"", digraph6.Graph("&F\\fPwYE`C?"), 2},
		{"", digraph6.Graph("&F\\fPw]C`A?"), 1},
		{"", digraph6.Graph("&F\\fPw]E_@?"), 1},
		{"", digraph6.Graph("&F\\FPwYE`c?"), 2},
		{"", digraph6.Graph("&FTFHg]Epo?"), 2},
		{"", digraph6.Graph("&FTFX_]Epg?"), 2},
		{"", digraph6.Graph("&FTfH_]EpW?"), 1},
		{"", digraph6.Graph("&FTFXgYEpc?"), 2},
		{"", digraph6.Graph("&FTfX_YEpK?"), 2},
		{"", digraph6.Graph("&FTfHgYEpS?"), 2},
		{"", digraph6.Graph("&FTFXg]Cpa?"), 2},
		{"", digraph6.Graph("&FTfHg]CpQ?"), 2},
		{"", digraph6.Graph("&FTfX_]CpI?"), 2},
		{"", digraph6.Graph("&FTfHg]EoP?"), 1},
		{"", digraph6.Graph("&FTFXg]Eo`?"), 1},
		{"", digraph6.Graph("&FTfXgYCpE?"), 2},
		{"", digraph6.Graph("&FTfX_]EoH?"), 2},
		{"", digraph6.Graph("&FTfXgYEoD?"), 1},
		{"", digraph6.Graph("&FTfXg]CoB?"), 1},
		{"", digraph6.Graph("&G^R{]Bo[B_Q?"), 1},
		{"", digraph6.Graph("&G^R{^B_[B_P?"), 1},
		{"", digraph6.Graph("&G^Rw^Bo[B_S?"), 1},
		{"", digraph6.Graph("&G^R{^BoWB_O_"), 1},
		{"", digraph6.Graph("&G^R{^Bo[A_OO"), 1},
		{"", digraph6.Graph("&G^bs^Bo[BOW?"), 1},
		{"", digraph6.Graph("&G^b{\\Bo[BGW?"), 1},
		{"", digraph6.Graph("&G^B{^Bo[B_W?"), 1},
		{"", digraph6.Graph("&G^b{^BO[BCW?"), 1},
		{"", digraph6.Graph("&G^b{^BoSBAW?"), 1},
		{"", digraph6.Graph("&G^R{^Bo[B_?G"), 1},
		{"", digraph6.Graph("&G]Rw^Bo]B_S?"), 1},
		{"", digraph6.Graph("&G]R{]Bo]B_Q?"), 1},
		{"", digraph6.Graph("&G]R{^B_]B_P?"), 1},
		{"", digraph6.Graph("&G]R{^Bo]A_OO"), 1},
		{"", digraph6.Graph("&G]B{^Bo]B_W?"), 1},
		{"", digraph6.Graph("&G]R{^BoYB_O_"), 1},
		{"", digraph6.Graph("&G]R{^Bo]B_?G"), 1},
		{"", digraph6.Graph("&G]B{^BoYB_W_"), 1},
		{"", digraph6.Graph("&G]B{^Bo]A_WO"), 1},
		{"", digraph6.Graph("&G]R{]BoYB_Q_"), 1},
		{"", digraph6.Graph("&G]Rw^Bo]A_SO"), 1},
		{"", digraph6.Graph("&G]R{]Bo]A_QO"), 1},
		{"", digraph6.Graph("&G]R{^B_]A_PO"), 1},
		{"", digraph6.Graph("&G]bs^Bo]BOW?"), 1},
		{"", digraph6.Graph("&G]ro^Bo]BOS?"), 1},
		{"", digraph6.Graph("&G]rs]Bo]BOQ?"), 2},
		{"", digraph6.Graph("&G]rs^B_]BOP?"), 2},
		{"", digraph6.Graph("&G]rs^Bo]AOOO"), 1},
		{"", digraph6.Graph("&G]rs^BoYBOO_"), 2},
		{"", digraph6.Graph("&G]rs^Bo]BO?G"), 1},
		{"", digraph6.Graph("&G]ro^BoYBOS_"), 1},
		{"", digraph6.Graph("&G]bs^BoYBOW_"), 2},
		{"", digraph6.Graph("&G]ro^Bo]AOSO"), 1},
		{"", digraph6.Graph("&G]rs]BoYBOQ_"), 2},
		{"", digraph6.Graph("&G]rs]Bo]AOQO"), 1},
		{"", digraph6.Graph("&G]b{\\Bo]BGW?"), 1},
		{"", digraph6.Graph("&G]rs^B_]AOPO"), 1},
		{"", digraph6.Graph("&G]rw\\Bo]BGS?"), 1},
		{"", digraph6.Graph("&G]r{[Bo]BGQ?"), 1},
		{"", digraph6.Graph("&G]r{\\B_]BGP?"), 2},
		{"", digraph6.Graph("&G]r{\\BoYBGO_"), 2},
		{"", digraph6.Graph("&G]r{\\Bo]AGOO"), 1},
		{"", digraph6.Graph("&G]r{\\Bo]BG?G"), 1},
		{"", digraph6.Graph("&G]r{[BoYBGQ_"), 1},
		{"", digraph6.Graph("&G]r{[Bo]AGQO"), 1},
		{"", digraph6.Graph("&G]b{^BO]BCW?"), 1},
		{"", digraph6.Graph("&G]r{\\B_]AGPO"), 1},

		{"Brinkmann", graph6.Graph("Ts_???BC?OGEGICO@OHGHP?oaOAAOGY_?UG?"), 6}, // graphs from "A SAT Approach to Twin width"
		{"Chvatal", graph6.Graph("Ks_HIGZKQSm_"), 3},
		{"Clebsch", graph6.Graph("OsaBA`GP@`dIHWEcas_]O"), 6},
		{"Desargues", graph6.Graph("Ss???[GA?C?_?_@?CCG`CGD?OWA?WA?E?"), 4},
		//{"Dodecahedron", graph6.Graph(""),4}
		{"Dürer", graph6.Graph("Kt?GGLAOOpDA"), 3},
		{"Errera", graph6.Graph("PThAIHAO[_P@KSIK_p_iTAhW"), 5},
		{"FlowerSnark", graph6.Graph("Ss??GOGA_I?c????GOQAACGO_P?_K@?S?"), 4},
		{"Folkman", graph6.Graph("Ss_???????GwPoHKAT?QoAU?Qg@H_Bo??"), 3},
		{"Franklin", graph6.Graph("Ks???wYP`KL?"), 2},
		{"Frucht", graph6.Graph("Kt?GWC@PGbL?"), 3},
		{"Goldner", graph6.Graph("J??CrJVjzv_"), 2},
		//{"Grid 6 × 8", graph6.Graph(""), 3},
		{"Grötzsch", graph6.Graph("J?AKagjXfo?"), 3},
		{"Herschel", graph6.Graph("JQQCA?tIo{?"), 2},
		{"Hoffman", graph6.Graph("Os_????PpJAsQoIWBSBg?"), 4},
		{"Holt", graph6.Graph("Zs_?GG?????E?I?c_H_`?AG?H??c?A?CC_CCO?QH?@aAa@?BAC?GS?_GP?_?"), 6},
		{"Kittell", graph6.Graph("Vzo?GDWCGG?W?A?@_?WK]?PWGo??TG?`]f??KSA[WOL?"), 5},
		{"McGee", graph6.Graph("Ws???CA?GA??AC@A@?_GGA??C?ICG?`?G`?GCAG@?__G?W?"), 4},
		{"Moser", graph6.Graph("F`o~_"), 2},
		{"Nauru", graph6.Graph("Ws?????A_H?oAOAG?g?A??G?A?A?_`?O`?CGC?H@?AGA?P?"), 4},
		//{"Paley-73", graph6.Graph(""), 36},
		{"Pappus", graph6.Graph("Qs??OGC@?O@?GgCcCG_aOOD@?g?"), 4},
		{"Petersen", graph6.Graph("IsP@OkWHG"), 4},
		{"Poussin", graph6.Graph("N@K?gW^oDcLG`ePT{B_"), 4},
		{"Robertson", graph6.Graph("Rs_?I@?O?`CAAOHQ@R@DCBH?RC?gc?"), 6},
		//{"Rook 6*6", graph6.Graph(""), 10},
		{"Shrikhande", graph6.Graph("O}akqPPWOV@iHIDHcROcj"), 6},
		{"Sousselier", graph6.Graph("O??_iA@_A?@PBCGTADVo?"), 4},
		{"Tietze", graph6.Graph("Kt?G?DIPOqCo"), 4},
		{"Wagner", graph6.Graph("GsOiho"), 2},
	}
	for _, tc := range testCases {
		if tc.name == "" {
			switch tc.graph.(type) {
			case graph6.Graph:
				tc.name = string(tc.graph.(graph6.Graph))
			case digraph6.Graph:
				tc.name = string(tc.graph.(digraph6.Graph))
			}
		}
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			result, sequence := TwinWidth(tc.graph)
			if result != tc.twinWidth {
				t.Errorf("%s: Twinwidth calculated wrongly: Got %d, expected %d", tc.name, result, tc.twinWidth)
			}
			if len(sequence) != tc.graph.Nodes().Len()-1 {
				t.Errorf("%s: Unexpected contraction sequence length: Got %v, expected %d", tc.name, sequence, tc.graph.Nodes().Len()-1)
			}
			var tg trigraph.Trigraph
			switch tc.graph.(type) {
			case graph.Directed:
				tg = trigraph.NewDiTrigraph(tc.graph.(graph.Directed))
			case graph.Undirected:
				tg = trigraph.NewUnTrigraph(tc.graph.(graph.Undirected))
			}
			if d := contraction.DegreeFromSequence(tg, sequence); d != result {
				t.Errorf("%s: Twinwidth recalculated from result sequence doesn't equal result: Got %d, expected %d", tc.name, d, result)
			}
		})
	}
}

// Test the tiny set of the PACE Challenge
func TestTinySet(t *testing.T) {
	testCases := []struct {
		file      string
		twinWidth int
	}{
		{"../gr/tiny-set/selfloop.gr", 1},
		{"../gr/tiny-set/comments.gr", 1},
		{"../gr/tiny-set/tiny001.gr", 1},
		{"../gr/tiny-set/tiny002.gr", 2},
		{"../gr/tiny-set/tiny003.gr", 0},
		{"../gr/tiny-set/tiny004.gr", 0},
		{"../gr/tiny-set/tiny005.gr", 3},
		{"../gr/tiny-set/tiny006.gr", 0},
		{"../gr/tiny-set/tiny007.gr", 2},
		{"../gr/tiny-set/tiny008.gr", 4},
		{"../gr/tiny-set/tiny009.gr", 1},
		{"../gr/tiny-set/tiny010.gr", 2},
	}
	for _, tc := range testCases {
		t.Run(tc.file, func(t *testing.T) {
			t.Parallel()
			graph := grParser.UnGraphFromDIMACSFile(tc.file)
			result, sequence := TwinWidth(graph)
			if result != tc.twinWidth {
				t.Errorf("Twinwidth calculated wrongly: Got %d, expected %d", result, tc.twinWidth)
			}
			if len(sequence) != graph.Nodes().Len()-1 {
				t.Errorf("Unexpected contraction sequence length: Got %v, expected %d", sequence, graph.Nodes().Len()-1)
			}
			tg := trigraph.NewUnTrigraph(graph)
			d := contraction.DegreeFromSequence(tg, sequence)
			if d != result {
				t.Errorf("%s: Twinwidth recalculated from result sequence doesn't equal result: Got %d, expected %d", tc.file, d, result)
			}
		})
	}
}

func TestPaceOutput(t *testing.T) {
	for i := 2; i <= 18; i += 2 {
		t.Run(fmt.Sprintf("exact_%03d.gr", i), func(t *testing.T) {
			t.Parallel()
			g := grParser.UnGraphFromDIMACSFile(fmt.Sprintf("../gr/exact-public/exact_%03d.gr", i))
			tww, sequence := TwinWidth(g)
			sequence.WriteToFile(fmt.Sprintf("../gr/exact-public/exact_%03d.tww", i))
			compareCommand := exec.Command("python", "verifier.py", fmt.Sprintf("../gr/exact-public/exact_%03d.gr", i), fmt.Sprintf("../gr/exact-public/exact_%03d.tww", i))
			output, err := compareCommand.Output()
			if err != nil {
				fmt.Println(output)
				t.Fatal(err)
			}
			verifiedTww, err := strconv.Atoi(strings.TrimSuffix(
				strings.Split(string(output[:]), " ")[1], "\n"))
			if err != nil {
				fmt.Println(output)
				t.Fatal(err)
			}
			if verifiedTww != tww {
				t.Errorf("Calculated Twinwidth %d does not equal verified twin width %d.", tww, verifiedTww)
			}
			tg := trigraph.NewUnTrigraph(g)
			d := contraction.DegreeFromSequence(tg, sequence)
			if d != tww {
				t.Errorf("Twinwidth recalculated from result sequence doesn't equal result: Got %d, expected %d", d, tww)
			}
		})
	}
}

// Test the tiny set of the PACE Challenge
func BenchmarkPACEPublic(b *testing.B) {
	for i := 2; i <= 200; i += 2 {
		b.Run(fmt.Sprintf("exact_%03d.gr", i), func(b *testing.B) {
			for j := 0; j < b.N; j++ {
				graph := grParser.UnGraphFromDIMACSFile(fmt.Sprintf("../gr/exact-public/exact_%03d.gr", i))
				TwinWidth(graph)
			}
		})
	}
}
