package tww

import (
	"testing"
)

func TestPermutationOf(t *testing.T) {
	p := Permutiation{[]int{1, 2, 3}, []int{4, 5}}
	tc := []int{0, 2, 3, 1, 5, 4}
	for test, res := range tc {
		if p.of(test) != res {
			t.Errorf("Wrong Permutation result: Got %d, but expected %d.", p.of(test), res)
		}
	}
}
