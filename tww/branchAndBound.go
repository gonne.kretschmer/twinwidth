// twinWidth.go
package tww

import (
	"twinWidth/contraction"
	"twinWidth/trigraph"

	"github.com/oleiade/lane"
	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/simple"
)

// Calculates lower bounds to tww by doing a breadth first search on the possible contraction.s
// terminates in after reaching depth of 1 or round, whatever is greater.
// Assumes, that queue exactly contains nodes of a given bnb-subtree at level 1.
func BFSWorker(trigraph trigraph.Trigraph, queue *lane.Queue, result *Result, smallestNode, largestNode int, quit chan (bool), rounds int) {
	var oldLength, min int
	workgraph := trigraph.Copy()
	for {
		select {
		case <-quit:
			return
		default:
			if !queue.Empty() {
				workgraph.Reset(trigraph)
				oldLength, min = BFSStep(workgraph, queue, result, smallestNode, largestNode, rounds, oldLength, min)
			} else {
				result.IncreaseMinimum(min)
				<-quit
				return
			}
		}
	}
}

// Calculates lower bounds to tww by doing a breadth first search on the possible contraction.s
// terminates in after reaching depth of 1 or round, whatever is greater.
// Assumes, that queue exactly contains nodes of a given bnb-subtree at level 1 or one node on level 0.
func BFSUnthreaded(trigraph trigraph.Trigraph, queue *lane.Queue, result *Result, rounds int, smallestNode int, largestNode int) {
	bfsQueue := BFSQueue(queue, smallestNode, largestNode, rounds)
	BFSChildren(trigraph, bfsQueue, result, smallestNode, largestNode)
}

// BFSQueue takes a queue, that exactly contains nodes of a given bnb-subtree at level 1 or one node on level 0,
// and turns into a queue of all contraction sequences of length (initial length + rounds -1)
func BFSQueue(queue *lane.Queue, smallestNode, largestNode, rounds int) *lane.Queue {
	res := lane.NewQueue()
	switch queue.Size() {
	case 0:
		break
	case 1:
		firstcontraction := queue.Dequeue() // get the starting length
		rounds = rounds + len(firstcontraction.(contraction.ContractionSequence))
		queue.Enqueue(firstcontraction)
	default:
		firstcontraction := queue.Dequeue() // get the starting length
		rounds = rounds + len(firstcontraction.(contraction.ContractionSequence)) - 1
		queue.Enqueue(firstcontraction)
	}
	for !queue.Empty() {
		sequence := queue.Dequeue().(contraction.ContractionSequence)
		if len(sequence) < rounds-1 && len(sequence) < largestNode-smallestNode-2 { // if sequence is not long enough, append children
			for i := smallestNode; i <= largestNode; i++ {
				for j := i + 1; j <= largestNode; j++ {
					illegalcontraction := false
					for _, contraction := range sequence {
						if i == contraction[1] || j == contraction[1] {
							illegalcontraction = true
						}
					}
					if illegalcontraction {
						continue
					}
					child := make(contraction.ContractionSequence, len(sequence)+1)
					copy(child, sequence)
					child[len(sequence)] = contraction.Contraction{i, j}
					queue.Enqueue(child)
				}
			}
		} else {
			res.Enqueue(sequence)
		}
	}
	return res
}

func BFSChildren(tg trigraph.Trigraph, queue *lane.Queue, result *Result, smallestNode, largestNode int) {
	workgraph := tg.Copy()
	cachegraph := tg.Copy()
	min := largestNode - smallestNode
	if !queue.Empty() {
		for !queue.Empty() {
			sequence := queue.Dequeue().(contraction.ContractionSequence)
			cachegraph.Reset(tg)
			d := contraction.DegreeFromSequence(cachegraph, sequence)
			if d < min {
				for i := smallestNode; i <= largestNode; i++ {
					for j := i + 1; j <= largestNode; j++ {
						illegalcontraction := false
						for _, contraction := range sequence {
							if i == contraction[1] || j == contraction[1] {
								illegalcontraction = true
							}
						}
						if illegalcontraction {
							continue
						}
						workgraph.Reset(cachegraph)
						childD := contraction.DegreeFromSequence(workgraph, contraction.ContractionSequence{contraction.Contraction{i, j}})
						if childD < d {
							childD = d
						}
						if childD < min {
							min = childD
						}
					}
				}
			}
		}
		result.IncreaseMinimum(min)
	}
}

func BFSStep(tg trigraph.Trigraph, queue *lane.Queue, result *Result, smallestNode, largestNode, rounds, oldLength, min int) (int, int) {
	sequence := queue.Dequeue().(contraction.ContractionSequence)
	if oldLength != len(sequence) {
		result.IncreaseMinimum(min)
		min = largestNode - smallestNode + 1
	}
	d := contraction.FastDegreeFromSequence(tg, sequence, largestNode-smallestNode)
	if d < min {
		min = d
	}
	if len(sequence) < rounds && len(sequence) < largestNode-smallestNode-1 {
		for i := smallestNode; i <= largestNode; i++ {
			for j := i + 1; j <= largestNode; j++ {
				illegalcontraction := false
				for _, contraction := range sequence {
					if i == contraction[1] || j == contraction[1] {
						illegalcontraction = true
					}
				}
				if illegalcontraction {
					continue
				}
				child := make(contraction.ContractionSequence, len(sequence)+1)
				copy(child, sequence)
				child[len(sequence)] = contraction.Contraction{i, j}
				queue.Enqueue(child)
			}
		}
	}
	oldLength = len(sequence)
	return oldLength, min
}

func PQWorker(g graph.Graph, tg trigraph.Trigraph, queue *lane.PQueue, result *Result, smallestNode int, largestNode int) {
	workgraph := tg.Copy()
	cachegraph := tg.Copy()
	// if we have at least 12 nodes, it is worth filtering out
	// partitions that we have already come along.
	cachePartitions := largestNode-smallestNode+1 >= 12 && largestNode-smallestNode+1 <= 2_000
	cacheMergeBelow := largestNode - smallestNode - 12
	for {
		s, priority := queue.Pop()
		if s == nil { // There are no contraction sequences left to try
			return
		}
		sequence := s.(contraction.ContractionSequence)
		degree := (largestNode-smallestNode+1)*len(sequence) - priority
		if result.Tww() <= result.Minimum() { // We have found an optimum solution
			return
		}
		if degree >= result.Tww() {
			continue
		} else if len(sequence) == largestNode-smallestNode {
			workgraph.Reset(tg)
			accepted := result.ImproveResult(contraction.FastDegreeFromSequence(workgraph, sequence, result.Tww()), sequence)
			if accepted {
				//fmt.Printf("Found new sequence with tww %d, %v, current minimum: %d\n", result.Tww(), result.Sequence(), result.Minimum())
				if result.Tww() <= result.Minimum() { // We have found an optimum solution
					return
				}
				BFSofSubgraphs(g, sequence, result)
			}
		} else {
			cachegraphInitialised := false
			for i := smallestNode; i <= largestNode; i++ {
				for j := i + 1; j <= largestNode; j++ {
					illegalcontraction := false
					for _, contraction := range sequence {
						if i == contraction[1] || j == contraction[1] {
							illegalcontraction = true
						}
					}
					if illegalcontraction {
						continue
					}
					child := make(contraction.ContractionSequence, len(sequence)+1)
					copy(child, sequence)
					child[len(sequence)] = contraction.Contraction{i, j}

					// Akzeptiere, wenn der übrige Graph klein genug ist oder
					// der nächste Schritt den Grad der contraction.ContractionSequence unter dem
					// bisherigen Optimum werden lässt.
					if len(child) >= largestNode-smallestNode-2 {
						queue.Push(child, (largestNode-smallestNode+1)*len(child)-degree)
					} else {
						var d int
						if len(sequence) < cacheMergeBelow {
							if !cachegraphInitialised {
								cachegraph.Reset(tg)
								contraction.DegreeFromSequence(cachegraph, sequence)
								cachegraphInitialised = true
							}
							workgraph.Reset(cachegraph)
							d = contraction.FastDegreeFromSequence(workgraph, contraction.ContractionSequence{contraction.Contraction{i, j}}, result.Tww())
						} else {
							workgraph.Reset(tg)
							d = contraction.FastDegreeFromSequence(workgraph, child, result.Tww())
						}
						if cachePartitions {
							partition := child.Partition(largestNode + 1)
							if d < result.Tww() && result.PartitionCache.Add(partition, d) {
								queue.Push(child, (largestNode-smallestNode+1)*len(child)-d)
							}
						} else if d < result.Tww() {
							queue.Push(child, (largestNode-smallestNode+1)*len(child)-d)
						}
					}
				}
			}
		}
	}
}

// PopulateWork first merges all twins in a graph and then
// gives a trigraph of the result.
func PopulateWork(g graph.Graph) (trigraph.Trigraph, *lane.Queue, *lane.PQueue, *Result, int, int) {
	smallestNode, largestNode := nodeBorders(g)
	result := NewResult(largestNode - smallestNode + 1)

	noTwins := removeTwins(g, result, smallestNode, largestNode)
	normalGraph, nodeMap := normalFromGraph(noTwins, 0)
	result.nodeMap = nodeMap

	var tg trigraph.Trigraph
	switch normalGraph.(type) {
	case graph.Directed:
		tg = trigraph.NewDiTrigraph(normalGraph.(graph.Directed))
	case graph.Undirected:
		tg = trigraph.NewUnTrigraph(normalGraph.(graph.Undirected))
	default:
		panic("Graph type unrecognized")
	}
	smallestNode, largestNode = nodeBorders(normalGraph)
	queue, pqueue := initialiseQueues(normalGraph, tg, smallestNode, largestNode)
	return tg, queue, pqueue, result, 0, normalGraph.Nodes().Len() - 1
}

// initialiseQueues pushes all contractions from which to try to find
// contraction sequences.
func initialiseQueues(g graph.Graph, tg trigraph.Trigraph, smallestNode, largestNode int) (*lane.Queue, *lane.PQueue) {
	queue := lane.NewQueue()
	pqueue := lane.NewPQueue(lane.MAXPQ)
	// Don't check symmetries on small graphs. The number is a wild guess.
	if largestNode-smallestNode+1 <= 15 {
		queue.Enqueue(contraction.ContractionSequence{})
		pqueue.Push(contraction.ContractionSequence{}, 0)
	} else {
		automorphisms := NewAutomorphismStore(g)
		if len(automorphisms.generators) == 0 {
			queue.Enqueue(contraction.ContractionSequence{})
			pqueue.Push(contraction.ContractionSequence{}, 0)
		} else {
			workgraph := tg.Copy()
			// this slice holds whether for i < j the contraction of nodes
			// i and j has already been enqueued (e.g. via symmetry)
			contracted := make([]bool, (largestNode+1)*largestNode/2)
			for i := smallestNode; i <= largestNode; i++ {
				for j := i + 1; j <= largestNode; j++ {
					if !contracted[j*(j-1)/2+i] {
						// Block all isomorphic pairs of vertices.
						for _, orbit := range automorphisms.PairOrbit(i, j) {
							contracted[orbit[1]*(orbit[1]-1)/2+orbit[0]] = true
						}
						// Append current vertex pair to queues.
						workgraph.Reset(tg)
						cs := contraction.ContractionSequence{contraction.Contraction{i, j}}
						queue.Enqueue(cs)
						pqueue.Push(cs, largestNode-smallestNode+1-contraction.DegreeFromSequence(workgraph, cs))
						contracted[j*(j-1)/2+i] = true
					}
				}
			}
		}
	}
	return queue, pqueue
}

// removeTwins removes all twins from the graph and returns the result.
// In doing this it also calculates a lower bound of level 1.
func removeTwins(g graph.Graph, result *Result, smallestNode, largestNode int) graph.Graph {
	lowerBound := largestNode

	type simpleGraph interface {
		graph.Builder
		graph.Graph
		graph.NodeRemover
	}
	var noTwins simpleGraph
	var tg trigraph.Trigraph
	switch g.(type) {
	case graph.Directed:
		tg = trigraph.NewDiTrigraph(g.(graph.Directed))
		noTwins = simple.NewDirectedGraph()
	case graph.Undirected:
		tg = trigraph.NewUnTrigraph(g.(graph.Undirected))
		noTwins = simple.NewUndirectedGraph()
	default:
		panic("Graph type unrecognized")
	}
	graph.Copy(noTwins, g)
	workgraph := tg.Copy()

	twinsFound := true
	prefixSequence := make(contraction.ContractionSequence, 0)
	for twinsFound && len(prefixSequence) <= largestNode-smallestNode {
		twinsFound = false
	searchTwin:
		for i := smallestNode; i <= largestNode; i++ {
			for j := i + 1; j <= largestNode; j++ {
				illegalcontraction := false
				for _, contraction := range prefixSequence {
					if i == contraction[1] || j == contraction[1] {
						illegalcontraction = true
					}
				}
				if illegalcontraction {
					continue
				}
				sequence := make(contraction.ContractionSequence, len(prefixSequence)+1)
				copy(sequence, prefixSequence)
				sequence[len(sequence)-1] = contraction.Contraction{i, j}
				workgraph.Reset(tg)
				d := contraction.FastDegreeFromSequence(workgraph, sequence, largestNode)
				if d == 0 {
					prefixSequence = sequence
					noTwins.RemoveNode(int64(j))

					if len(sequence) == largestNode-smallestNode { // only push leaves of work tree, else push all children
						result.ImproveResult(0, contraction.ContractionSequence{})
						lowerBound = 0
					} else {
						lowerBound = largestNode
					}
					twinsFound = true
					break searchTwin
				}
				if lowerBound > d {
					lowerBound = d
				}
			}
		}
	}
	result.IncreaseMinimum(lowerBound)
	result.prefix = prefixSequence
	return noTwins
}

// nodeBorders calculates the smallest node-id and the largest node-id in a graph.
func nodeBorders(g graph.Graph) (int, int) {
	smallestNode := g.Nodes().Len()
	largestNode := 0
	nodes := g.Nodes()
	for nodes.Next() {
		node := nodes.Node()
		if int(node.ID()) < smallestNode {
			smallestNode = int(node.ID())
		}
		if int(node.ID()) > largestNode {
			largestNode = int(node.ID())
		}
	}
	return smallestNode, largestNode
}

// Takes a graph with arbitrary node numbers and
// converts it to an isomorphic graph of nodes from startID to startID+n-1.
// Also returns a map that maps the normal id to the original id.
func normalFromGraph(g graph.Graph, startID int) (graph.Graph, map[int]int64) {
	type simpleGraph interface {
		graph.Builder
		graph.Graph
		graph.NodeRemover
	}
	var normalGraph simpleGraph
	switch g.(type) {
	case graph.Directed:
		normalGraph = simple.NewDirectedGraph()
	case graph.Undirected:
		normalGraph = simple.NewUndirectedGraph()
	}
	nextID := startID
	gToNormal := make(map[int64]int)
	normalToG := make(map[int]int64)
	nodes := g.Nodes()
	for nodes.Next() { // add all nodes of this connected component
		node := nodes.Node()
		gToNormal[node.ID()] = nextID
		normalToG[nextID] = node.ID()
		normalGraph.AddNode(simple.Node(nextID))
		nextID++
	}
	nodes.Reset()
	for nodes.Next() { // add all edges
		from := nodes.Node()
		neighbours := g.From(from.ID())
		for neighbours.Next() {
			to := neighbours.Node()
			normalGraph.SetEdge(normalGraph.NewEdge(normalGraph.Node(int64(gToNormal[from.ID()])), normalGraph.Node(int64(gToNormal[to.ID()]))))
		}
	}
	return normalGraph, normalToG
}

func PopulateWorkConnectedComponents(g graph.Undirected, cs contraction.ContractionSequence) (trigraph.Trigraph, *lane.PQueue, *Result, int, int) {
	smallestNode := g.Nodes().Len()
	largestNode := 0
	nodes := g.Nodes()
	for nodes.Next() {
		node := nodes.Node()
		if int(node.ID()) < smallestNode {
			smallestNode = int(node.ID())
		}
		if int(node.ID()) > largestNode {
			largestNode = int(node.ID())
		}
	}
	result := NewResult(largestNode - smallestNode + 1)

	tg := trigraph.NewUnTrigraph(g.(graph.Undirected))
	pqueue := lane.NewPQueue(lane.MAXPQ)

	pqueue.Push(cs, (largestNode-smallestNode+1)*len(cs))

	return tg, pqueue, result, smallestNode, largestNode
}

func BFSofSubgraphs(g graph.Graph, sequence contraction.ContractionSequence, result *Result) {
	type simpleGraph interface {
		graph.Builder
		graph.Graph
		graph.NodeRemover
	}
	var subgraph simpleGraph
	switch g.(type) {
	case graph.Directed:
		subgraph = simple.NewDirectedGraph()
	case graph.Undirected:
		subgraph = simple.NewUndirectedGraph()
	}
	graph.Copy(subgraph, g)
removeNodes:
	for i := 0; i < len(sequence)-5; i += 1 {
		subgraph.RemoveNode(int64(sequence[i][1]))

		var workgraph trigraph.Trigraph
		normalSubgraph, _ := normalFromGraph(subgraph, 0)
		switch normalSubgraph.(type) {
		case graph.Directed:
			workgraph = trigraph.NewDiTrigraph(normalSubgraph.(graph.Directed))
		case graph.Undirected:
			workgraph = trigraph.NewUnTrigraph(normalSubgraph.(graph.Undirected))
		}
		queue := lane.NewQueue()
		queue.Append(contraction.ContractionSequence{})
		switch {
		case workgraph.MaxNodes()-1 <= 15:
			break removeNodes
		case workgraph.MaxNodes()-1 <= 110:
			BFSUnthreaded(workgraph, queue, result, 2, 0, workgraph.MaxNodes()-1)
		case workgraph.MaxNodes()-1 <= 800:
			BFSUnthreaded(workgraph, queue, result, 1, 0, workgraph.MaxNodes()-1)
		}
	}
}
