package tww

import (
	"math"
	"twinWidth/contraction"
)

type Result struct {
	s              contraction.ContractionSequence // a contraction sequence belonging to the currently best found tww on the graph.
	prefix         contraction.ContractionSequence // a prefix that is not calculated in the branch and bound section and not affected by nodeMap.
	tww            int                             // The currently lowest d for which a d-sequence is found. Becomes the twin-width if calculation is completed.
	minimum        int                             // The minimum twin width found by doing a breadth first search on the possible contractions.
	PartitionCache contraction.PartitionCache      // Cache partitions in order to not compute them multiple times.
	nodeMap        map[int]int64                   // nodeMap maps an node id from the trigraph being worked on to the original id. They may differ after removing twins.
}

func NewResult(vertices int) *Result {
	nodeMap := make(map[int]int64)
	for i := 0; i <= vertices; i++ {
		nodeMap[i] = int64(i)
	}
	// https://arxiv.org/abs/2110.03957 Bounds for the Twin-width of Graphs
	maxTww := int(math.Floor((float64(vertices) + math.Sqrt(float64(vertices)*math.Log(float64(vertices))) + math.Sqrt(float64(vertices)) + 2*math.Log(float64(vertices))) / 2))
	return &Result{make(contraction.ContractionSequence, 0), make(contraction.ContractionSequence, 0), maxTww, 0, contraction.NewPartitionTree(), nodeMap}
}

func (r *Result) Tww() int {
	return r.tww
}

func (r *Result) Minimum() int {
	return r.minimum
}

// Return a contraction sequence belonging to the currently best found tww on the graph.
func (r *Result) Sequence() contraction.ContractionSequence {
	original := make(contraction.ContractionSequence, 0)
	for i, _ := range r.s {
		original = append(original, contraction.Contraction{int(r.nodeMap[r.s[i][0]]), int(r.nodeMap[r.s[i][1]])})
	}
	return append(r.prefix, original...)
}

// Improve the result with a given twin-width and sequence.
// If the new twin-width is larger than an earlier result, don't do anything.
// Returns if the result was accepted.
func (r *Result) ImproveResult(tww int, sequence contraction.ContractionSequence) bool {
	if tww >= r.tww {
		return false
	}
	r.tww = tww
	if len(r.s) != len(sequence) {
		r.s = make(contraction.ContractionSequence, len(sequence))
	}
	copy(r.s, sequence)
	return true
}

func (r *Result) IncreaseMinimum(min int) {
	if min > r.minimum {
		r.minimum = min
	}
}
