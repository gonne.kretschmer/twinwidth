package tww

import (
	"os/exec"
	"path"
	"runtime"
	"testing"

	"gonum.org/v1/gonum/graph"

	"gonum.org/v1/gonum/graph/encoding/graph6"
)

func BenchmarkNamedGraphs(b *testing.B) {
	testCases := []struct {
		name      string
		graph     graph.Graph
		twinWidth int
	}{
		{"Brinkmann", graph6.Graph("Ts_???BC?OGEGICO@OHGHP?oaOAAOGY_?UG?"), 6}, // graphs from "A SAT Approach to Twin width"
		{"Chvatal", graph6.Graph("Ks_HIGZKQSm_"), 3},
		{"Clebsch", graph6.Graph("OsaBA`GP@`dIHWEcas_]O"), 6},
		{"Desargues", graph6.Graph("Ss???[GA?C?_?_@?CCG`CGD?OWA?WA?E?"), 4},
		//{"Dodecahedron", graph6.Graph(""),4}
		{"Dürer", graph6.Graph("Kt?GGLAOOpDA"), 3},
		{"Errera", graph6.Graph("PThAIHAO[_P@KSIK_p_iTAhW"), 5},
		{"FlowerSnark", graph6.Graph("Ss??GOGA_I?c????GOQAACGO_P?_K@?S?"), 4},
		{"Folkman", graph6.Graph("Ss_???????GwPoHKAT?QoAU?Qg@H_Bo??"), 3},
		{"Franklin", graph6.Graph("Ks???wYP`KL?"), 2},
		{"Frucht", graph6.Graph("Kt?GWC@PGbL?"), 3},
		{"Goldner", graph6.Graph("J??CrJVjzv_"), 2},
		//{"Grid 6 × 8", graph6.Graph(""), 3},
		{"Grötzsch", graph6.Graph("J?AKagjXfo?"), 3},
		{"Herschel", graph6.Graph("JQQCA?tIo{?"), 2},
		{"Hoffman", graph6.Graph("Os_????PpJAsQoIWBSBg?"), 4},
		{"Holt", graph6.Graph("Zs_?GG?????E?I?c_H_`?AG?H??c?A?CC_CCO?QH?@aAa@?BAC?GS?_GP?_?"), 6},
		{"Kittell", graph6.Graph("Vzo?GDWCGG?W?A?@_?WK]?PWGo??TG?`]f??KSA[WOL?"), 5},
		{"McGee", graph6.Graph("Ws???CA?GA??AC@A@?_GGA??C?ICG?`?G`?GCAG@?__G?W?"), 4},
		{"Moser", graph6.Graph("F`o~_"), 2},
		{"Nauru", graph6.Graph("Ws?????A_H?oAOAG?g?A??G?A?A?_`?O`?CGC?H@?AGA?P?"), 4},
		/*{"Paley-9", graphProvider.Paley9(), 0},
		{"Paley-25", graphProvider.Paley25(), 0},
		{"Paley-37", graphProvider.Paley37(), 0},
		{"Paley-41", graphProvider.Paley41(), 0},
		{"Paley-49", graphProvider.Paley49(), 0},
		{"Paley-53", graphProvider.Paley53(), 0},
		{"Paley-61", graphProvider.Paley61(), 0},
		{"Paley-73", graphProvider.Paley73(), 0},*/
		{"Pappus", graph6.Graph("Qs??OGC@?O@?GgCcCG_aOOD@?g?"), 4},
		{"Petersen", graph6.Graph("IsP@OkWHG"), 4},
		{"Poussin", graph6.Graph("N@K?gW^oDcLG`ePT{B_"), 4},
		{"Robertson", graph6.Graph("Rs_?I@?O?`CAAOHQ@R@DCBH?RC?gc?"), 6},
		//{"Rook 6*6", graph6.Graph(""), 10},
		{"Shrikhande", graph6.Graph("O}akqPPWOV@iHIDHcROcj"), 6},
		{"Sousselier", graph6.Graph("O??_iA@_A?@PBCGTADVo?"), 4},
		{"Tietze", graph6.Graph("Kt?G?DIPOqCo"), 4},
		{"Wagner", graph6.Graph("GsOiho"), 2},
	}
	for _, tc := range testCases {
		b.Run("TWW "+tc.name, func(b *testing.B) {
			for j := 0; j < b.N; j++ {
				TwinWidth(tc.graph)
			}
		})
	}
	_, file, _, _ := runtime.Caller(0)
	for _, tc := range testCases {
		b.Run("SAT "+tc.name, func(b *testing.B) {
			for j := 0; j < b.N; j++ {
				cmd := exec.Command("python3", path.Dir(file)+"/../../twin_width/runner.py", string(tc.graph.(graph6.Graph)))
				cmd.Run()
			}
		})
	}
}
