package tww

import (
	"bufio"
	"bytes"
	"os/exec"
	"path"
	"runtime"
	"strconv"
	"strings"
	"twinWidth/graphProvider/grParser"

	"github.com/oleiade/lane"

	"gonum.org/v1/gonum/graph"
)

type AutomorphismStore struct {
	generators []Permutiation
}

type Permutiation [][]int // Permutiation in cycle notation

func NewAutomorphismStore(g graph.Graph) *AutomorphismStore {
	generators := make([]Permutiation, 0)
	normalGraph, nodeMap := normalFromGraph(g, 1)
	dimacs := grParser.DIMACSFromGraph(normalGraph)
	_, file, _, _ := runtime.Caller(0)
	var autoCommand *exec.Cmd
	switch g.(type) {
	case graph.Directed:
		autoCommand = exec.Command(path.Dir(file)+"/../../bliss-0.77/bliss", "-directed")
	case graph.Undirected:
		autoCommand = exec.Command(path.Dir(file) + "/../../bliss-0.77/bliss")
	}
	autoCommand.Stdin = strings.NewReader(dimacs)
	output, _ := autoCommand.Output()
	outputBuffer := bufio.NewReader(bytes.NewBuffer(output))
	var (
		line string
		err  error
	)
	for err == nil {
		line, err = outputBuffer.ReadString('\n')
		line = strings.Trim(line, "\n")
		if len(line) >= 12 && line[:12] == "Generator: (" {
			line = strings.TrimPrefix(line, "Generator: ")
			cycles := strings.Split(line, ")")
			generator := make(Permutiation, 0, len(cycles))
			for _, cycleStr := range cycles {
				cycleStr := strings.TrimPrefix(cycleStr, "(")
				if cycleStr != "" {

					numbers := strings.Split(cycleStr, ",")
					cycle := make([]int, 0, len(numbers))
					for _, numStr := range numbers {
						if numStr != "" {
							num, _ := strconv.Atoi(numStr)
							cycle = append(cycle, int(nodeMap[num]))
						}
					}
					if len(cycle) > 0 {
						generator = append(generator, cycle)
					}
				}
			}
			if len(generator) > 0 {
				generators = append(generators, generator)
			}
		} else { // Generators are only at the start. None will follow now.
			break
		}
	}
	return &AutomorphismStore{generators}
}

// PairOrbit returns the 2-Orbit of a pair of nodes,
// except the original pair.
// It is normalized such that the smaller node is the first and does not contain duplicates.
func (a *AutomorphismStore) PairOrbit(u, v int) [][2]int {
	result := make([][2]int, 0, len(a.generators)+1)
	if u > v {
		u, v = v, u
	}
	result = append(result, [2]int{u, v})
	workqueue := lane.NewQueue()
	workqueue.Enqueue([2]int{u, v})
	for !workqueue.Empty() {
		pair := workqueue.Pop().([2]int)
	gen:
		for _, p := range a.generators {
			x, y := p.of(pair[0]), p.of(pair[1])
			if x > y {
				x, y = y, x
			}
			for i, _ := range result {
				if result[i][0] == x && result[i][1] == y {
					continue gen
				}
			}
			result = append(result, [2]int{x, y})
			workqueue.Enqueue([2]int{x, y})
		}
	}
	return result[1:] // don't return (u,v)
}

func (p Permutiation) of(x int) int {
	result := x
search:
	for i, _ := range p {
		for j, _ := range p[i] {
			if p[i][j] == x {
				result = p[i][(j+1)%len(p[i])]
				break search
			}
		}
	}
	return result
}
