// twinWidth.go
package tww

import (
	"twinWidth/contraction"

	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/simple"
	"gonum.org/v1/gonum/graph/topo"
)

// Calculate if the twin width is at least a given lower bound.
// Intended to find twin width numbers as defined in "A SAT Approach to Twin-Width".
func TwinWidthAbove(g graph.Graph, lowerBound int, falseOnTwins bool) (bool, contraction.ContractionSequence) {

	trigraph, _, pqueue, result, smallestNode, largestNode := PopulateWork(g)
	result.IncreaseMinimum(lowerBound - 1) // Early return on graphs where a d-sequence for lower d exists

	if falseOnTwins && len(result.prefix) != 0 {
		return false, contraction.ContractionSequence{}
	}

	if result.Minimum() >= lowerBound {
		// Ugly hack: We haven't found a minimal sequence, but we know, that we won't go below the threshhold.
		result.ImproveResult(result.Minimum(), contraction.ContractionSequence{})
	}
	PQWorker(g, trigraph, pqueue, result, smallestNode, largestNode)

	return result.Tww() >= lowerBound, result.Sequence()
}

// TwinWidth calculates the twin width of a given graph.
// It is expected that the node ids of the graph are consecutive,
// but they don't need to start at 0.
func TwinWidth(g graph.Graph) (int, contraction.ContractionSequence) {
	switch g.(type) {
	case graph.Directed:
		return TwinWidthConnected(g)
	case graph.Undirected:
		sequence := make(contraction.ContractionSequence, 0)
		var originalComponentSize, complementComponentSize int
		for _, component := range topo.ConnectedComponents(g.(graph.Undirected)) {
			if originalComponentSize < len(component) {
				originalComponentSize = len(component)
			}
		}
		complement := simple.NewUndirectedGraph()
		nodes := g.Nodes()
		for nodes.Next() {
			complement.AddNode(nodes.Node())
		}
		nodes.Reset()
		for nodes.Next() {
			u := nodes.Node()
			to := g.Nodes()
			for to.Next() {
				v := to.Node()
				if g.Edge(u.ID(), v.ID()) == nil && u.ID() != v.ID() {
					complement.SetEdge(simple.Edge{F: u, T: v})
				}
			}
		}
		for _, component := range topo.ConnectedComponents(complement) {
			if complementComponentSize < len(component) {
				complementComponentSize = len(component)
			}
		}
		if originalComponentSize > complementComponentSize {
			g = complement
		}
		var tww int
		for _, component := range topo.ConnectedComponents(g.(graph.Undirected)) {
			cc := simple.NewUndirectedGraph()
			var nextID int
			gToCC := make(map[int64]int)
			ccToG := make(map[int]int64)
			for _, node := range component { // add all nodes of this connected component
				gToCC[node.ID()] = nextID
				ccToG[nextID] = node.ID()
				cc.AddNode(simple.Node(nextID))
				nextID++
			}
			for _, from := range component { // add all edges of this connected component
				neighbours := g.From(from.ID())
				for neighbours.Next() {
					to := neighbours.Node()
					cc.SetEdge(cc.NewEdge(cc.Node(int64(gToCC[from.ID()])), cc.Node(int64(gToCC[to.ID()]))))
				}
			}
			ccTww, ccSequence := TwinWidthConnected(cc)
			for i, _ := range ccSequence {
				sequence = append(sequence, contraction.Contraction{int(ccToG[ccSequence[i][0]]), int(ccToG[ccSequence[i][1]])})
			}
			if ccTww > tww {
				tww = ccTww
			}
		}
		tg, pq, result, smallestNode, largestNode := PopulateWorkConnectedComponents(g.(graph.Undirected), sequence)
		result.IncreaseMinimum(tww)
		PQWorker(g, tg, pq, result, smallestNode, largestNode)
		return result.Tww(), result.Sequence()
	default:
		return 0, contraction.ContractionSequence{}
	}
}

// TwinWidthConnected calculates the twin width of a given graph.
// It also works on unconnected graphs but no optimisations are applied in this respect.
// It is expected that the node ids of the graph are consecutive,
// but they don't need to start at 0.
func TwinWidthConnected(g graph.Graph) (int, contraction.ContractionSequence) {
	trigraph, queue, pqueue, result, smallestNode, largestNode := PopulateWork(g)
	switch nodes := largestNode - smallestNode + 1; {
	case nodes <= 15:
		BFSUnthreaded(trigraph, queue, result, 2, smallestNode, largestNode)
	case nodes <= 110:
		BFSUnthreaded(trigraph, queue, result, 3, smallestNode, largestNode)
	case nodes <= 800:
		BFSUnthreaded(trigraph, queue, result, 2, smallestNode, largestNode)
	}

	PQWorker(g, trigraph, pqueue, result, smallestNode, largestNode)
	return result.Tww(), result.Sequence()
}
