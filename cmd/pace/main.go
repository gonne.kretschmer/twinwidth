// twinWidth
package main

import (
	"bufio"
	"fmt"
	"os"
	"twinWidth/graphProvider/grParser"
	"twinWidth/tww"
)

func main() {
	g := grParser.UnGraphFromDIMACSScanner(bufio.NewScanner(os.Stdin))
	_, sequence := tww.TwinWidth(g)
	fmt.Print(sequence.PaceFormat())
}
