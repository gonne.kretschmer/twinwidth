// twinWidth
package main

import (
	"flag"
	"log"
	"os"
	"runtime/pprof"
	"time"
	"twinWidth/graphProvider"
)

var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")
var stop = flag.Bool("stop", false, "stop early")

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		go func() {
			time.Sleep(2 * time.Minute)
			pprof.StopCPUProfile()
			os.Exit(0)
		}()
	} else if *stop {
		go func() {
			time.Sleep(2 * time.Minute)
			os.Exit(0)
		}()
	}
	/*passFile := make(chan string, 2)
	go func() {
		for i := 70; i >= 20; i -= 2 {
			if i <= 20 || i > 20 || i == 34 || i == 44 || i == 50 || i == 56 || i == 62 || i == 66 || i == 24 {
				passFile <- fmt.Sprintf("gr/exact-public/exact_%03d.gr", i)
			}
		}
		close(passFile)
	}()
	graphProvider.CalulateMassFile(passFile, true)*/

	//	g := grParser.UnGraphFromDIMACS("gr/exact-public/exact_024.gr")
	//	tww, sequence := tww.TwinWidth(g)
	//	sequence.WriteToFile("gr/exact-public/exact_024.tww")
	graphProvider.CalculateGraphs([]int{3, 4, 5, 6, 7, 8, 9, 10 /*, 11, 12, 13, 14, 15 /* , 16, 17, 18, 19, 20, 21, 22, 23, 24, 25*/}, false)
	//fmt.Println(sequence, tww)
	//calulateTournaments([]int{8}, false)
	//calulateGraphs([]int{4}, true)
	//TwinWidthNumber(5, true)
}
